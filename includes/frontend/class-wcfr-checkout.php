<?php

use FramaNL\Model\Consignment\AbstractConsignment;
use FramaNL\Model\Consignment\PostNLConsignment;
use FramaNL\Support\Arr;
use WPO\WC\Frama\Compatibility\Order as WCX_Order;
use WPO\WC\Frama\Compatibility\WC_Core as WCX;

if (! defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

if (class_exists('WCFR_Checkout')) {
    return new WCFR_Checkout();
}

/**
 * Frontend views
 */
class WCFR_Checkout
{
    private const DELIVERY_OPTIONS_KEY_MAP = [
        'deliveryType'                   => 'delivery_type',
        'isPickup'                       => 'is_pickup',
        'labelDescription'               => 'label_description',
        'pickupLocation'                 => 'pickup_location',
        'packageType'                    => 'package_type',
        'shipmentOptions'                => 'shipment_options',
        'shipmentOptions.ageCheck'       => 'shipment_options.age_check',
        'shipmentOptions.insuredAmount'  => 'shipment_options.insured_amount',
        'shipmentOptions.largeFormat'    => 'shipment_options.large_format',
        'shipmentOptions.onlyRecipient'  => 'shipment_options.only_recipient',
        'shipmentOptions.returnShipment' => 'shipment_options.return_shipment',
    ];

    /**
     * WCFR_Checkout constructor.
     */
    public function __construct()
    {
        add_action("wp_enqueue_scripts", [$this, "enqueue_frontend_scripts"], 100);

        // Save delivery options data
        add_action("woocommerce_checkout_update_order_meta", [$this, "save_delivery_options"], 10, 2);

        add_action("wp_ajax_wcframa_get_delivery_options_config", [$this, "getDeliveryOptionsConfigAjax"]);
    }

    /**
     * Load styles & scripts on the checkout page.
     *
     * @throws \Exception
     */
    public function enqueue_frontend_scripts(): void
    {
        // The order received page has the same page id as the checkout so `is_checkout()` returns true on both...
        if (! is_checkout() || is_order_received_page()) {
            return;
        }

        // if using split address fields
        $useSplitAddressFields = WCFRAMA()->setting_collection->isEnabled(WCFRAMA_Settings::SETTING_USE_SPLIT_ADDRESS_FIELDS);
        if ($useSplitAddressFields) {
            wp_enqueue_script(
                "wcframa-checkout-fields",
                WCFRAMA()->plugin_url() . "/assets/js/wcframa-checkout-fields.js",
                ["wc-checkout"],
                WC_FRAMA_NL_VERSION,
                true
            );
        }

        // Don"t load the delivery options scripts if it"s disabled
        if (! WCFRAMA()->setting_collection->isEnabled(WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_ENABLED)) {
            return;
        }

        /**
         * JS dependencies array
         */
        $deps = ["wc-checkout"];

        /**
         * If split address fields are enabled add the checkout fields script as an additional dependency.
         */
        if ($useSplitAddressFields) {
            $deps[] = "wcframa-checkout-fields";
        }

        /*
         * Show delivery options also for shipments on backorder
         */
        if (! $this->shouldShowDeliveryOptions()) {
            return;
        }

        wp_enqueue_script(
            "wc-frama",
            WCFRAMA()->plugin_url() . "/assets/js/frama-pickup-points.js",
            $deps,
            WC_FRAMA_NL_VERSION,
            true
        );

        wp_enqueue_script(
            "wc-frama-frontend",
            WCFRAMA()->plugin_url() . "/assets/js/wcframa-frontend.js",
            array_merge($deps, ["wc-frama", "jquery"]),
            WC_FRAMA_NL_VERSION,
            true
        );

        $this->inject_delivery_options_variables();
    }

    /**
     * Localize variables into the delivery options scripts.
     *
     * @throws Exception
     */
    public function inject_delivery_options_variables(): void
    {
        wp_localize_script(
            'wc-frama-frontend',
            'wcframa',
            [
                "ajax_url" => admin_url("admin-ajax.php"),
            ]
        );

        wp_localize_script(
            "wc-frama-frontend",
            "FramaDisplaySettings",
            [
                // Convert true/false to int for JavaScript
                "isUsingSplitAddressFields" => (int) WCFRAMA()->setting_collection->isEnabled(
                    WCFRAMA_Settings::SETTING_USE_SPLIT_ADDRESS_FIELDS
                ),
                "splitAddressFieldsCountries" => WCFR_NL_Postcode_Fields::COUNTRIES_WITH_SPLIT_ADDRESS_FIELDS,
                "pickupLocationCountries" => WCFRAMA_Settings::COUNTRIES_WITH_PICKUP_LOCATIONS,
            ]
        );

        wp_localize_script(
            "wc-frama",
            "FramaDeliveryOptions",
            [
                "allowedShippingMethods"    => json_encode($this->getShippingMethodsAllowingDeliveryOptions()),
                "disallowedShippingMethods" => json_encode(WCFR_Export::DISALLOWED_SHIPPING_METHODS),
                "alwaysShow"                => $this->alwaysDisplayDeliveryOptions(),
                "hiddenInputName"           => WCFRAMA_Admin::META_DELIVERY_OPTIONS,
            ]
        );

        wp_localize_script(
            'wc-frama',
            'FramaConfig',
            $this->getDeliveryOptionsConfig()
        );

        // Load the checkout template.
        add_action(
            apply_filters(
                'wc_wcframa_delivery_options_location',
                WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_POSITION)
            ),
            [$this, 'output_delivery_options'],
            10
        );
    }

    /**
     * @return string
     */
    public function get_delivery_options_shipping_methods()
    {
        $packageTypes = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_SHIPPING_METHODS_PACKAGE_TYPES);

        if (! is_array($packageTypes)) {
            $packageTypes = [];
        }

        $shipping_methods = [];

        if (array_key_exists(AbstractConsignment::PACKAGE_TYPE_PACKAGE, $packageTypes ?? [])) {
            // settings_checkout_display_for_selected_methods = enable delivery options
            $shipping_methods = $packageTypes[AbstractConsignment::PACKAGE_TYPE_PACKAGE];
        }

        return json_encode($shipping_methods);
    }

    /**
     * Get the delivery options config in JSON for passing to JavaScript.
     *
     * @return array
     */
    public function getDeliveryOptionsConfig(): array
    {
        $settings                  = WCFRAMA()->setting_collection;
        $carriers                  = $this->getCarriers();
        $cartTotals                = WC()->session->get('cart_totals');
        $chosenShippingMethodPrice = (float) $cartTotals['shipping_total'];
        $displayIncludingTax       = WC()->cart->display_prices_including_tax();

        if ($displayIncludingTax) {
            $chosenShippingMethodPrice += (float) $cartTotals['shipping_tax'];
        }

        $framaConfig = [
            "config" => [
                "currency"           => get_woocommerce_currency(),
                "locale"             => "nl-NL",
                "platform"           => "frama",
                "basePrice"          => $chosenShippingMethodPrice,
            ],
            "strings" => [
                "addressNotFound"       => __("Address details are not entered", "frama-woocommerce"),
                "city"                  => __("City", "frama-woocommerce"),
                "closed"                => __("Closed", "frama-woocommerce"),
                "deliveryStandardTitle" => self::getDeliveryOptionsTitle(WCFRAMA_Settings::SETTING_STANDARD_TITLE),
                "deliveryTitle"         => self::getDeliveryOptionsTitle(WCFRAMA_Settings::SETTING_DELIVERY_TITLE),
                "headerDeliveryOptions" => self::getDeliveryOptionsTitle(WCFRAMA_Settings::SETTING_HEADER_DELIVERY_OPTIONS_TITLE),
                "houseNumber"           => __("House number", "frama-woocommerce"),
                "onlyRecipientTitle"    => self::getDeliveryOptionsTitle(WCFRAMA_Settings::SETTING_ONLY_RECIPIENT_TITLE),
                "openingHours"          => __("Opening hours", "frama-woocommerce"),
                "pickUpFrom"            => __("Pick up from", "frama-woocommerce"),
                "pickupTitle"           => self::getDeliveryOptionsTitle(WCFRAMA_Settings::SETTING_PICKUP_TITLE),
                "postcode"              => __("Postcode", "frama-woocommerce"),
                "retry"                 => __("Retry", "frama-woocommerce"),
                "signatureTitle"        => self::getDeliveryOptionsTitle(WCFRAMA_Settings::SETTING_SIGNATURE_TITLE),
                "wrongHouseNumberCity"  => __("Postcode/city combination unknown", "frama-woocommerce"),
            ],
        ];

        foreach ($carriers as $carrier) {
            foreach (self::getDeliveryOptionsConfigMap($carrier) as $key => $setting) {
                [$settingName, $function, $addBasePrice] = $setting;

                $value = $settings->{$function}($carrier . '_' . $settingName);

                if (is_numeric($value) && $this->useTotalPrice() && $addBasePrice) {
                    $value += $chosenShippingMethodPrice;
                }

                Arr::set($framaConfig, 'config.' . $key, $value);
            }
        }

        $framaConfig['config']['priceStandardDelivery'] = $this->useTotalPrice() ? $chosenShippingMethodPrice : null;

        return $framaConfig;
    }

    /**
     * Echoes the delivery options config as a JSON string for use with AJAX.
     */
    public function getDeliveryOptionsConfigAjax(): void
    {
        echo json_encode($this->getDeliveryOptionsConfig(), JSON_UNESCAPED_SLASHES);
        die();
    }

    /**
     * @return bool
     */
    public function useTotalPrice(): bool
    {
        return false;
    }

    /**
     * @param string $title
     *
     * @return string
     */
    public static function getDeliveryOptionsTitle(string $title): string
    {
        $settings = WCFRAMA()->setting_collection;

        return __(strip_tags($settings->getStringByName($title)), "frama-woocommerce");
    }

    /**
     * Output the delivery options template.
     */
    public function output_delivery_options(): void
    {
        do_action('woocommerce_frama_before_delivery_options');
        require_once(WCFRAMA()->includes . '/views/html-delivery-options-template.php');
        do_action('woocommerce_frama_after_delivery_options');
    }

    /**
     * Get the array of enabled carriers by checking if they have either delivery or pickup enabled.
     *
     * @return array
     */
    private function getCarriers(): array
    {
        $settings = WCFRAMA()->setting_collection;
        $carriers = [];

        foreach ([PostNLConsignment::CARRIER_NAME] as $carrier) {
            if ($settings->getByName("{$carrier}_" . WCFRAMA_Settings::SETTING_CARRIER_PICKUP_ENABLED)
                || $settings->getByName(
                    "{$carrier}_" . WCFRAMA_Settings::SETTING_CARRIER_DELIVERY_ENABLED
                )) {
                $carriers[] = $carrier;
            }
        }

        return $carriers;
    }

    /**
     * Save delivery options to order when used
     *
     * @param int   $order_id
     * @param array $posted
     *
     * @return void
     * @throws Exception
     */
    public static function save_delivery_options($order_id)
    {
        $order = WCX::get_order($order_id);

        $shippingMethod       = Arr::get($_POST, "shipping_method");
        $highestShippingClass = Arr::get($_POST, "frama_highest_shipping_class") ?? $shippingMethod[0];

        /**
         * Save the current version of our plugin to the order.
         */
        WCX_Order::update_meta_data(
            $order,
            WCFRAMA_Admin::META_ORDER_VERSION,
            WCFRAMA()->version
        );

        WCX_Order::update_meta_data(
            $order,
            WCFRAMA_Admin::META_SHIPMENT_OPTIONS_EXTRA,
            [
                'collo_amount' => 1,
                'weight'       => WC()->cart->get_cart_contents_weight(),
            ]
        );

        if ($highestShippingClass) {
            WCX_Order::update_meta_data(
                $order,
                WCFRAMA_Admin::META_HIGHEST_SHIPPING_CLASS,
                $highestShippingClass
            );
        }

        $pickupLocationFromPost = Arr::get($_POST, WCFRAMA_Admin::META_PICKUP_LOCATION);
        if ($pickupLocationFromPost) {
            WCX_Order::update_meta_data($order, WCFRAMA_Admin::META_PICKUP_LOCATION, stripslashes($pickupLocationFromPost));
        }

        $deliveryOptionsFromPost          = Arr::get($_POST, WCFRAMA_Admin::META_DELIVERY_OPTIONS);
        $deliveryOptionsFromShippingClass = $highestShippingClass
            ? [
                'packageType' => WCFR_Export::getPackageTypeFromShippingMethod(
                    $shippingMethod[0],
                    $highestShippingClass
                ),
            ]
            : null;

        $deliveryOptions = empty($deliveryOptionsFromPost)
            ? $deliveryOptionsFromShippingClass
            : stripslashes($deliveryOptionsFromPost);

        if ($deliveryOptions) {
            if (! is_array($deliveryOptions)) {
                $deliveryOptions = json_decode($deliveryOptions, true);
            }
            $deliveryOptions = self::convertDeliveryOptionsForAdapter($deliveryOptions);
            $deliveryOptions = WCFRAMA_Admin::removeDisallowedDeliveryOptions(
                $deliveryOptions,
                $order->get_shipping_country()
            );

            /*
             * Create a new DeliveryOptions class from the data.
             */
            $deliveryOptions = new WCFR_DeliveryOptionsFromOrderAdapter(null, $deliveryOptions);

            /*
             * Store it in the meta data.
             */
            WCX_Order::update_meta_data(
                $order,
                WCFRAMA_Admin::META_DELIVERY_OPTIONS,
                $deliveryOptions->toArray()
            );
        }


    }

    /**
     * Return the names of shipping methods that will show delivery options. If DISPLAY_FOR_ALL_METHODS is enabled it'll
     * return an empty array and the frontend will allow any shipping except any that are specifically disallowed.
     *
     * @return string[]
     * @throws Exception
     * @see WCFR_Export::DISALLOWED_SHIPPING_METHODS
     */
    private function getShippingMethodsAllowingDeliveryOptions(): array
    {
        $allowedMethods = [];
        $displayFor     = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_DISPLAY);

        if (WCFR_Settings_Data::DISPLAY_FOR_ALL_METHODS === $displayFor) {
            return $allowedMethods;
        }

        $shippingMethodsByPackageType = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_SHIPPING_METHODS_PACKAGE_TYPES);
        $shippingMethodsForPackage    = $shippingMethodsByPackageType[AbstractConsignment::PACKAGE_TYPE_PACKAGE_NAME];

        foreach ($shippingMethodsForPackage as $shippingMethod) {
            [$methodId] = self::splitShippingMethodString($shippingMethod);

            if (!in_array($methodId, WCFR_Export::DISALLOWED_SHIPPING_METHODS)) {
                $allowedMethods[] = $shippingMethod;
            }
        }

        return $allowedMethods;
    }

    /**
     * @return bool
     */
    private function alwaysDisplayDeliveryOptions(): bool
    {
        $display = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_DISPLAY);

        return $display === WCFR_Settings_Data::DISPLAY_FOR_ALL_METHODS;
    }

    /**
     * Split a <rateId>:<instanceId> string into an array. If there is no instanceId, the second array element will be
     * null.
     *
     * @param $shippingMethod
     *
     * @return array
     */
    public static function splitShippingMethodString(string $shippingMethod): array
    {
        $split = explode(':', $shippingMethod, 2);

        if (count($split) === 1) {
            $split[] = null;
        }

        return $split;
    }

    /**
     * Map keys from the delivery options to the keys used in the adapters.
     *
     * @param array $deliveryOptions
     *
     * @return array
     */
    private static function convertDeliveryOptionsForAdapter(array $deliveryOptions): array
    {
        foreach (self::DELIVERY_OPTIONS_KEY_MAP as $camel => $snake) {
            $value = Arr::get($deliveryOptions, $camel);
            if (isset($value)) {
                Arr::set($deliveryOptions, $snake, $value);
                Arr::forget($deliveryOptions, $camel);
            }
        }

        return $deliveryOptions;
    }

    /**
     * @param string $carrier
     *
     * @return array[]
     */
    private static function getDeliveryOptionsConfigMap(string $carrier): array
    {
        return [
           "carrierSettings.$carrier.allowDeliveryOptions"  => [WCFRAMA_Settings::SETTING_CARRIER_DELIVERY_ENABLED, 'isEnabled', false],
           "carrierSettings.$carrier.allowOnlyRecipient"    => [WCFRAMA_Settings::SETTING_CARRIER_ONLY_RECIPIENT_ENABLED, 'isEnabled', false],
           "carrierSettings.$carrier.allowPickupLocations"  => [WCFRAMA_Settings::SETTING_CARRIER_PICKUP_ENABLED, 'isEnabled', false],
           "carrierSettings.$carrier.allowSignature"        => [WCFRAMA_Settings::SETTING_CARRIER_SIGNATURE_ENABLED, 'isEnabled', false],
           "carrierSettings.$carrier.priceOnlyRecipient"    => [WCFRAMA_Settings::SETTING_CARRIER_ONLY_RECIPIENT_FEE, 'getPriceByName', false],
           "carrierSettings.$carrier.pricePickup"           => [WCFRAMA_Settings::SETTING_CARRIER_PICKUP_FEE, 'getPriceByName', true],
           "carrierSettings.$carrier.priceSignature"        => [WCFRAMA_Settings::SETTING_CARRIER_SIGNATURE_FEE, 'getPriceByName', false],
       ];
    }

    /**
     * Show delivery options also for shipments on backorder
     * @return bool
     */
    private function shouldShowDeliveryOptions(): bool
    {
        $show                     = true;

        foreach (WC()->cart->get_cart() as $cartItem) {
            /**
             * @var WC_Product $product
             */
            $product       = $cartItem['data'];
            $isOnBackorder = $product->is_on_backorder($cartItem['quantity']);

            if ($isOnBackorder) {
                $show = false;
                break;
            }
        }

        return $show;
    }
}

return new WCFR_Checkout();
