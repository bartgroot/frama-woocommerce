<?php

use FramaNL\Support\Arr;
use WPO\WC\Frama\Compatibility\WC_Core as WCX;
use WPO\WC\Frama\Compatibility\Order as WCX_Order;

if (! defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

if (class_exists('WCFR_Frontend')) {
    return new WCFR_Frontend();
}

/**
 * Frontend views
 */
class WCFR_Frontend
{
    public function __construct()
    {
        new WCFR_Frontend_Track_Trace();

		// Shipment information in confirmation mail
	    add_action("woocommerce_email_customer_details", [$this, "confirmationEmail"], 19, 3);

	    // Shipment information in my account
	    add_action('woocommerce_view_order', [$this, "confirmationOrderReceived"]);

	    // Shipment information on the thank you page
	    add_action("woocommerce_thankyou", [$this, "confirmationOrderReceived"], 10, 1);
        add_filter("wpo_wcpdf_templates_replace_frama_delivery_options", [$this, "wpo_wcpdf_delivery_options"], 10, 2);

        // Initialize delivery options fees
        new WCFR_Cart_Fees();

        // Output most expensive shipping class in frontend data
        add_action("woocommerce_checkout_before_order_review", [$this, "injectShippingClassInput"], 100);
        add_action("woocommerce_update_order_review_fragments", [$this, "order_review_fragments"]);

        // Ajax
        add_action('wp_ajax_get_highest_shipping_class', [$this, 'ajaxGetHighestShippingClass']);
        add_action('wp_ajax_nopriv_get_highest_shipping_class', [$this, 'ajaxGetHighestShippingClass']);

        add_action('wp_ajax_frama_get_pickup_locations', [$this, 'ajaxGetPickupLocations']);
        add_action('wp_ajax_nopriv_frama_get_pickup_locations', [$this, 'ajaxGetPickupLocations']);
    }

    /**
     * @param \WC_Order $order
     *
     * @throws \Exception
     */
    public function confirmationEmail(WC_Order $order): void
    {
	    WCFRAMA()->admin->showShipmentConfirmation($order, true);
    }

    /**
     * @param int $order_id
     *
     * @throws Exception
     */
    public function confirmationOrderReceived(int $order_id): void
    {
        $order = wc_get_order($order_id);
        WCFRAMA()->admin->showShipmentConfirmation($order, false);
    }

    /**
     * @param string   $replacement
     * @param WC_Order $order
     *
     * @return string
     * @throws Exception
     */
    public function wpo_wcpdf_delivery_options(string $replacement, WC_Order $order): string
    {
        ob_start();
        WCFRAMA()->admin->showShipmentConfirmation($order, false);

        return ob_get_clean();
    }


    /**
     * Output the highest shipping class input
     *
     * @throws Exception
     */
    public function injectShippingClassInput(): void
    {
        echo '<div class="wcframa__shipping-data">';
        $this->renderHighestShippingClassInput();
        echo '</div>';
    }

    /**
     * @return string|void
     * @throws Exception
     */
    public function renderHighestShippingClassInput()
    {
        $shipping_class = WCFR_Frontend::get_cart_shipping_class();

        if ($shipping_class) {
            return sprintf(
                '<input type="hidden" value="%s" name="frama_highest_shipping_class">',
                $shipping_class
            );
        }
    }

    /**
     * Get the most expensive shipping class in the cart
     * Requires WC2.4+
     * Only supports 1 package, takes the first
     *
     * @return null|int
     * @throws \Exception
     */
    public static function get_cart_shipping_class(): ?int
    {
        if (version_compare(WOOCOMMERCE_VERSION, '2.4', '<')) {
            return null;
        }

        $shippingMethodString = WC()->session->get('chosen_shipping_methods')[0] ?? '';
        $shippingMethod       = WCFR_Export::getShippingMethod($shippingMethodString);

        if (empty($shippingMethod)) {
            return null;
        }

        if (method_exists($shippingMethod, 'find_shipping_classes')) {
            // get package
            $packages = WC()->cart->get_shipping_packages();
            $package  = current($packages);

            // get shipping classes from package
            $shippingClasses = $shippingMethod->find_shipping_classes($package);
        } else {
            $shippingClasses = [];
        }

        return WCFRAMA()->export->getShippingClass(
            $shippingMethod,
            $shippingClasses
        );
    }

    /**
     * @param $fragments
     *
     * @return mixed
     */
    public function order_review_fragments($fragments)
    {
        $frama_shipping_data            = $this->renderHighestShippingClassInput();
        $fragments['.wcframa__shipping-data'] = $frama_shipping_data;

        return $fragments;
    }

    /**
     * @param $order_id
     *
     * @return array|bool|mixed|void
     * @throws Exception
     */
    public static function getTrackTraceShipments($order_id): array
    {
        $order     = WCX::get_order($order_id);
        $shipments = WCFRAMA_Admin::get_order_shipments($order);

        if (empty($shipments)) {
            return [];
        }

        foreach ($shipments as $shipment_id => $shipment) {
            $track_trace_url = $shipment["trackTraceUrl"] ?? null;
            $barcode = $shipment["barcode"] ?? null;

            // skip concepts
            if (! $track_trace_url) {
                unset($shipments[$shipment_id]);
                continue;
            }

            // add links & urls
            Arr::set($shipments, "$shipment_id.track_trace_url", $track_trace_url);
            Arr::set(
                $shipments,
                "$shipment_id.track_trace_link",
                sprintf(
                    '<a href="%s">%s</a>',
                    $track_trace_url,
                    $barcode
                )
            );
        }

        return $shipments;
    }

    /**
     * @param $order_id
     *
     * @return array|bool
     * @throws Exception
     */
    public static function getTrackTraceLinks($order_id): array
    {
        $track_trace_links = [];

        $consignments = self::getTrackTraceShipments($order_id);

        foreach ($consignments as $key => $consignment) {
            $track_trace_links[] = [
                "link" => $consignment["track_trace_link"],
                "url"  => $consignment["track_trace_url"],
            ];
        }

        return $track_trace_links;
    }

    /**
     * @return int|null
     * @throws Exception
     */
    public function ajaxGetHighestShippingClass(): ?int
    {
        echo WCFR_Frontend::get_cart_shipping_class();
        die();
    }

    /**
     * @throws ErrorException
     * @throws Exception
     */
    public function ajaxGetPickupLocations() {
        $key = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_API_KEY);

        if (! ($key)) {
            throw new ErrorException(__("No API key found in Frama settings", "frama-woocommerce"));
        }

        $api = new WCFR_API($key);

        try {
            $resultArray = $api->getPickupLocations($_POST['cc'], $_POST['postcode']);
            if ($resultArray['code'] >= 400) {
                if (isset($resultArray['body']['traceId'])) {
                    $errorMessage = ($resultArray['body']['title'] ?? '') . ' TraceId: ' . $resultArray['body']['traceId'];
                } else {
                    if (is_array($resultArray['body']['error'] ?? false)) {
                        if (isset($resultArray['body']['error']['code'])) {
                            $errorMessage = $resultArray['body']['error']['code'] . ': ' . ($resultArray['body']['error']['message']??'');
                        }
                        else $errorMessage = print_r($resultArray['body']['error'], true);
                    }
                    else $errorMessage = ($resultArray['body']['error'] ?? 'Unknown error');
                    if (is_array($resultArray['body']['errors'] ?? false)) {
                        foreach ($resultArray['body']['errors'] as $errorData) {
                            $errorMessage .= ' ' . $errorData['error'];
                            if (isset($errorData['description'])) {
                                $errorMessage .= ' (' . $errorData['description'] . ')';
                            }
                        }
                    }
                }
                throw new ErrorException($errorMessage);
            }
            if (is_array($resultArray['body']['locations'])){
                if (empty($resultArray['body']['locations'])) {
                    echo __('No pickup locations found', "frama-woocommerce");
                }
                else {
                    $resultLocations = $resultArray['body']['locations'];
                    echo '<ul class="frama-pickup-locations">';
                    $locationCounter = 0;
                    foreach ($resultLocations as $resultLocation) {
                        $locationCounter++;
                        if ($locationCounter > 10) break; //return a maximum of 10 pickup locations
                        $address = $resultLocation['address']??[];
                        $id = $address['zipcode'] . $address['number'] . $address['numberSuffix'];
                        $meta_address = [
                            'name' => $resultLocation['location'],
                            'street' => $address['street'],
                            'number' => $address['number'],
                            'numberSuffix' => $address['numberSuffix'],
                            'zipcode' => $address['zipcode'],
                            'city' => $address['city'],
                            'countryCode' => $address['countryCode'],
                        ];
                        ?>
                        <li>
                            <input type="radio" name="_frama_pickup_location" value='<?= json_encode($meta_address) ?>' id="frama_pickup_location_<?= $id ?>" class="frama_pickup_location_radio">
                            <label for="frama_pickup_location_<?= $id ?>">
                                <span class="frama-pickup-title"><?= $resultLocation['location'] . ' (' . round((intval($resultLocation['distance'])/1000), 1) . ' km)';?></span>
                                <p class="frama-pickup-address"><?= $address['street'] . ' ' . $address['number'] . ' ' . $address['numberSuffix'] . ', ' . $address['zipcode'] . ' ' . $address['city'] ?></p>
                            </label>
                        </li>
                    <?php }
                    echo '</ul>';
                }
            } else {
                echo __('No pickup locations found', "frama-woocommerce");
            }
            wp_die();
        }
        catch (Exception $e) {
            wp_die('Error: '.$e->getMessage());
        }
    }
}

return new WCFR_Frontend();
