<div class="frama-woocommerce__delivery-options">
    <?php // Add custom css to the delivery options, if any
    if (!empty(WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_CUSTOM_CSS))) {
        echo "<style>";
        echo WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_CUSTOM_CSS);
        echo "</style>";
    } ?>

    <div id="frama_delivery_options"></div>
</div>
