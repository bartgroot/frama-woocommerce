<?php

use WPO\WC\Frama\Compatibility\Order as WCX_Order;
use WPO\WC\Frama\Compatibility\WC_Core;

if (! defined("ABSPATH")) {
    exit;
} // Exit if accessed directly

if (class_exists('WCFR_API')) {
    return;
}

class WCFR_API extends WCFR_Rest
{
    const PREFIX_PDF_FILENAME = 'frama-label-';

    /**
     * @var string
     */
    public $apiUrl = WCFR_Data::API_URL;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $userAgent;
    /**
     * @var false|string
     */
    private $label_pdf;

    /**
     * Default constructor
     *
     * @param string $key API Key provided by Frama
     *
     * @throws Exception
     */
    public function __construct($key)
    {
        parent::__construct();

        $this->apiUrl    = WCFR_Data::API_URL;
        $this->userAgent = $this->getUserAgent();
        $this->key       = (string) $key;
    }

    /**
     * Add shipment
     *
     * @param array $shipments array of shipments
     * @return array
     * @throws Exception
     */
    public function export_shipments(array $shipments): array
    {
        $endpoint = "plugin/shipment";

        $json = json_encode($shipments);

        $headers = [
            "Content-type"  => "application/json",
            "ApiKey" => $this->key,
            "Api-Version" => "1.0",
            "user-agent"    => $this->userAgent,
        ];

        $request_url = $this->apiUrl . $endpoint;

        return $this->post($request_url, $json, $headers);
    }

    /**
     * Get Wordpress, WooCommerce, Frama version and place theme in a array. Implode the array to get an UserAgent.
     *
     * @return string
     */
    private function getUserAgent(): string
    {
        $userAgents = [
            'Wordpress',
            get_bloginfo('version')
            . 'WooCommerce/'
            . WOOCOMMERCE_VERSION
            . 'FramaNL-WooCommerce/'
            . WC_FRAMA_NL_VERSION,
        ];

        // Place white space between the array elements
        return implode(" ", $userAgents);
    }

    /**
     * Get shipment labels, save them to the orders before showing them.
     *
     * @param array $shipment_ids Shipment ids.
     * @param array $order_ids
     * @param int $offset
     * @param bool $openInNewTab Download or display.
     *
     * @return void
     * @throws Exception
     */
    public function getShipmentLabels(array $shipment_ids, array $order_ids, int $offset = 0, bool $openInNewTab = true)
    {
        $endpoint = "plugin/label";

        // Determine the output int we send to the API: 1 = Label10x15, 2 = PositionsA4_A, 3 = PositionsA4_B, 4 = PositionsA4_C, 5 = PositionsA4_D, 6 = Doculop, 7 = DirectPrint
        $labelSetting = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_LABEL_FORMAT);
        if ($labelSetting === "A6") {
            $output = 1;
        }
        elseif ($labelSetting == 'DP') {
            $output = 7;
        }
        elseif ($labelSetting == 'A4') {
            $output = 2 + ($offset % 4);
        }
        else $output = 6;


        $json = json_encode(
            [
            "output" => $output,
            "ids" => $shipment_ids
            ]);

        WCFR_Log::add("Getting shipment labels, sending json: ".$json);

        $headers = [
            "Content-type"  => "application/json",
            "ApiKey" => $this->key,
            "Api-Version" => "1.0",
            "user-agent"    => $this->userAgent,
        ];

        $request_url = $this->apiUrl . $endpoint;

        $resultArray = $this->post($request_url, $json, $headers);

        $labels = $resultArray['body']['labels']; // array, e.g.: [[id] => 1180059, [barcode] => 3SFRAM691573387, [trackTraceUrl] => https://jouw.postnl.nl/track-and-trace/3SFRAM691573387-NL-4205KT]
        WCFR_Log::add("Labels: " . print_r($labels, true));
        if (isset($resultArray['body']['contentPDF'])) {
            $this->label_pdf = base64_decode($resultArray['body']['contentPDF']);
            $this->updateOrderBarcode($order_ids, $labels);
            if ($labelSetting !== 'DP') {
                $this->downloadPdfOfLabels($openInNewTab);
            }
        }
    }

    /**
     * Download labels
     *
     * @param bool $openInNewTab
     *
     * @return void
     * @throws Exception
     */
    private function downloadPdfOfLabels(bool $openInNewTab)
    {
        if ($this->label_pdf == null) {
            throw new Exception('First set label_pdf key with setPdfOfLabels() before running downloadPdfOfLabels()');
        }
        if (!$openInNewTab) { //force download with Content-disposition: inline; works
            header('Content-Type: application/force-download');
        }
        else header('Content-Type: application/pdf');
        header('Content-Length: ' . strlen($this->label_pdf));
        header('Content-disposition: inline; filename="' . self::PREFIX_PDF_FILENAME . gmdate('Y-M-d_H-i-s') . '.pdf"'); // Content-disposition: attachment; doesn't work in AJAX calls
        header('Cache-Control: public, must-revalidate, max-age=0');
        header('Pragma: no-cache');
        header('Expires: 0');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        echo $this->label_pdf;
        die();
        exit;
    }

    /**
     * Update the status of given order based on the automatic order status settings.
     *
     * @param WC_Order $order
     */
    public function updateOrderStatus(WC_Order $order): void
    {
        $statusAutomation     = WCFRAMA()->setting_collection->isEnabled(WCFRAMA_Settings::SETTING_ORDER_STATUS_AUTOMATION);
        $newStatus            = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_AUTOMATIC_ORDER_STATUS);

        if ($statusAutomation) {
            $order->update_status(
                $newStatus,
                __("frama_shipment_created", "frama-woocommerce")
            );

            WCFR_Log::add("Status of order {$order->get_id()} updated to \"$newStatus\"");
        }
    }

    /**
     * @param array $orderIds
     * @param array $labels
     * @throws JsonException
     */
    private function updateOrderBarcode(array $orderIds, array $labels): void
    {
        foreach ($orderIds as $orderId) {
            $order           = WC_Core::get_order($orderId);
            $lastShipmentIds = WCX_Order::get_meta($order, WCFRAMA_Admin::META_SHIPMENTS);

            if (empty($lastShipmentIds)) {
                continue;
            }
        }

        //WCFR_Log::add("saving Track&Traces for Orders ".print_r($orderIds, true));
        WCFR_Export::saveTrackTracesToOrders($labels, $orderIds);

        // do the order automation
        foreach ($orderIds as $orderId) {
            $order = WC_Core::get_order($orderId);
            $this->updateOrderStatus($order);
        }
    }

    /**
     * @throws Exception
     */
    public function getPickupLocations($country, $postcode): array
    {
        $endpoint = "plugin/pickup/".sanitize_key($country)."/".sanitize_key($postcode);

        $headers = [
            "Content-type"  => "application/json",
            "ApiKey" => $this->key,
            "Api-Version" => "1.0",
            "user-agent" => $this->userAgent,
        ];

        $request_url = $this->apiUrl . $endpoint;
//        return wp_remote_get($request_url, $headers);
        return $this->get($request_url, $headers);
    }
}
