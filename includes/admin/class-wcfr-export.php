<?php

use FramaNL\Adapter\DeliveryOptions\AbstractDeliveryOptionsAdapter;
use FramaNL\Exception\ApiException;
use FramaNL\Model\Consignment\AbstractConsignment;
use FramaNL\Model\Consignment\BPostConsignment;
use FramaNL\Model\Consignment\PostNLConsignment;
use FramaNL\Model\Consignment\UPSConsignment;
use FramaNL\Support\Str;
use WPO\WC\Frama\Compatibility\Order as WCX_Order;
use WPO\WC\Frama\Compatibility\WC_Core as WCX;

if (! defined("ABSPATH")) {
    exit;
} // Exit if accessed directly

if (class_exists("WCFR_Export")) {
    return new WCFR_Export();
}

class WCFR_Export
{
    public const EXPORT = "wcframa_export";

    public const ADD_SHIPMENTS = "add_shipments";
    public const GET_LABELS    = "get_labels";
    public const MODAL_DIALOG  = "modal_dialog";

    /**
     * Maximum characters length of item description.
     */
    public const ITEM_DESCRIPTION_MAX_LENGTH  = 50;
    public const ORDER_DESCRIPTION_MAX_LENGTH = 45;

    public const COOKIE_EXPIRE_TIME = 20;

    public const DEFAULT_POSITIONS = [2, 4, 1, 3];
    public const SUFFIX_CHECK_REG  = "~^([a-z]{1}\d{1,3}|-\d{1,4}\d{2}\w{1,2}|[a-z]{1}[a-z\s]{0,3})(?:\W|$)~i";

    /**
     * Shipping methods that can never have delivery options.
     */
    public const DISALLOWED_SHIPPING_METHODS = [
        WCFR_Shipping_Methods::LOCAL_PICKUP,
    ];

    public const COUNTRY_CODE_NL = 'NL';
    public const COUNTRY_CODE_BE = 'BE';

    public $order_id;
    public $success;
    public $errors;

    private $prefix_message;

    public function __construct()
    {
        $this->success = [];
        $this->errors  = [];

        require_once("class-wcfr-rest.php");
        require_once("class-wcfr-api.php");

        add_action("admin_notices", [$this, "admin_notices"]);

        add_action("wp_ajax_" . self::EXPORT, [$this, "export"]);
    }

    /**
     * @param int $orderId
     *
     * @throws ApiException
     * @throws ErrorException
     */
    public function exportByOrderId(int $orderId): void
    {
        $automaticExport = WCFRAMA()->setting_collection->isEnabled(WCFRAMA_Settings::SETTING_AUTOMATIC_EXPORT);

        if ($orderId && $automaticExport) {
            $export = new self();
            $export->addShipments([(string) $orderId], 0, false);
        }
    }

    /**
     * Get the value of a shipment option. Check if it was set manually, through the delivery options for example,
     *  if not get the value of the default export setting for given settingName.
     *
     * @param mixed  $option      Chosen value.
     * @param string $settingName Name of the setting to fall back to if there is no chosen value.
     *
     * @return mixed
     */
    public static function getChosenOrDefaultShipmentOption($option, string $settingName)
    {
        return $option ?? WCFRAMA()->setting_collection->getByName($settingName);
    }

    /**
     * @param $item
     * @param $order
     *
     * @return mixed|string
     */
    public static function get_item_display_name($item, $order)
    {
        // set base name
        $name = $item['name'];

        // add variation name if available
        $product = $order->get_product_from_item($item);
        if ($product && isset($item['variation_id']) && $item['variation_id'] > 0 && method_exists($product, 'get_variation_attributes')) {
            $name .= woocommerce_get_formatted_variation($product->get_variation_attributes());
        }

        return $name;
    }

    public function admin_notices()
    {
        // only do this when the user that initiated this
        if (isset($_GET["frama_done"])) {
            $action_return = get_option("wcframa_admin_notices");
            $print_queue   = get_option("wcframa_print_queue", []);
            $error_notice  = get_option("wcframa_admin_error_notices");

            if (! empty($action_return)) {
                foreach ($action_return as $type => $message) {
                    if (! in_array($type, ["success", "error"])) {
                        continue;
                    }

                    if ($type === "success" && ! empty($print_queue)) {
                        $print_queue_store = sprintf(
                            '<input type="hidden" value=\'%s\' class="wcframa__print-queue">',
                            json_encode(
                                [
                                    "shipment_ids" => $print_queue["shipment_ids"],
                                    "order_ids"    => $print_queue["order_ids"],
                                    "offset"       => $print_queue["offset"],
                                ]
                            )
                        );

                        // Empty queue
                        delete_option("wcframa_print_queue");
                    }

                    printf(
                        '<div class="wcframa__notice is-dismissible notice notice-%s"><p>%s</p>%s</div>',
                        $type,
                        $message,
                        $print_queue_store ?? ""
                    );
                }
                // destroy after reading
                delete_option("wcframa_admin_notices");
                wp_cache_delete("wcframa_admin_notices", "options");
            }
        }

        if (! empty($error_notice)) {
            printf(
                '<div class="wcframa__notice is-dismissible notice notice-error"><p>%s</p>%s</div>',
                $error_notice,
                $print_queue_store ?? ""
            );
            // destroy after reading
            delete_option("wcframa_admin_error_notices");
            wp_cache_delete("wcframa_admin_error_notices", "options");
        }

        if (isset($_GET["frama"])) {
            switch ($_GET["frama"]) {
                case "no_consignments":
                    $message = __(
                        "You have to export the orders to Frama before you can print the labels!",
                        "frama-woocommerce"
                    );
                    printf('<div class="wcframa__notice is-dismissible notice notice-error"><p>%s</p></div>', $message);
                    break;
                default:
                    break;
            }
        }

        if (isset($_COOKIE['frama_response'])) {
            $response = $_COOKIE['frama_response'];
            printf(
                '<div class="wcframa__notice is-dismissible notice notice-error"><p>%s</p></div>',
                $response
            );
        }
    }

    /**
     * Export selected orders.
     *
     * @access public
     * @return void
     * @throws Exception
     */
    public function export()
    {
        // Check the nonce
        if (! check_ajax_referer(WCFRAMA::NONCE_ACTION, "_wpnonce", false)) {
            die("Ajax security check failed. Did you pass a valid nonce in \$_REQUEST['_wpnonce']?");
        }

        if (! is_user_logged_in()) {
            wp_die(__("You do not have sufficient permissions to access this page.", "frama-woocommerce"));
        }

        $return = [];

        // Check the user privileges (maybe use order ids for filter?)
        if (apply_filters(
            "wc_frama_check_privs",
            ! current_user_can("manage_woocommerce_orders") && ! current_user_can("edit_shop_orders")
        )) {
            $return["error"] = __(
                "You do not have sufficient permissions to access this page.",
                "frama-woocommerce"
            );
            echo json_encode($return);
            die();
        }

        $dialog  = $_REQUEST["dialog"] ?? null;
        $print   = $_REQUEST["print"] ?? null;
        $offset  = (int) ($_REQUEST["offset"] ?? 0);
        $request = $_REQUEST["request"];

        /**
         * @var $order_ids
         */
        $order_ids    = $this->sanitize_posted_array($_REQUEST["order_ids"] ?? []);
        $shipment_ids = $this->sanitize_posted_array($_REQUEST["shipment_ids"] ?? []);

        if (empty($shipment_ids) && empty($order_ids)) {
            $this->errors[] = __("You have not selected any orders!", "frama-woocommerce");
        } else {
            if (!empty($shipment_ids)) {
                $shipment_ids_only_keys = [];
                foreach ($shipment_ids as $orderId => $shipment_id) {
                    $shipment_ids_only_keys[] = $shipment_id;
                }
                $shipment_ids = $shipment_ids_only_keys;
            }
            try {
                switch ($request) {
                    // Creating consignments.
                    case self::ADD_SHIPMENTS:
                        $this->addShipments($order_ids, $offset, $print);
                        break;

                    // Downloading labels.
                    case self::GET_LABELS:
                        $return = $this->printLabels($order_ids, $shipment_ids, $offset);
                        break;

                    case self::MODAL_DIALOG:
                        $order_ids = $this->filterOrderDestinations($order_ids);
                        $this->modal_dialog($order_ids, $dialog);
                        break;
                }
            } catch (Exception $e) {
                $errorMessage = $e->getMessage();
                $this->errors[] = "$request: {$errorMessage}";
                add_option("wcframa_admin_error_notices", $errorMessage);
            }
        }

        // display errors directly if PDF requested or modal
        if (! empty($this->errors) && in_array($request, [self::GET_LABELS, self::MODAL_DIALOG])) {
            echo $this->parse_errors($this->errors);
            die();
        }

        // format errors for html output
        if (! empty($this->errors)) {
            $return["error"] = $this->parse_errors($this->errors);
        }

        // if we're directed here from modal, show proper result page
        if (isset($_REQUEST["modal"])) {
            $this->modal_success_page($request, $return);
        } else {
            // return JSON response
            echo json_encode($return);
            die();
        }
    }

    /**
     * @param string|array $array
     *
     * @return array
     */
    public function sanitize_posted_array($array): array
    {
        if (is_array($array)) {
            return $array;
        }

        // check for JSON
        if (is_string($array) && strpos($array, "[") !== false) {
            $array = json_decode(stripslashes($array));
        }

        return (array) $array;
    }

    /**
     * @param array $order_ids
     *
     * @return array
     * @throws Exception
     */
    public function add_shipments(array $order_ids): array
    {
        $return          = [];

        WCFR_Log::add("*** Creating shipments started ***");

        $shipments = [];
        $shipmentsByOrderId = [];
        /**
         * Loop over the order ids and create consignments for each order.
         */
        foreach ($order_ids as $order_id) {

            $order = WCX::get_order($order_id);

            try {
                $shipment = $this->prepareShipmentData($order_id, $order);
                $shipments[] = $shipment;
                $shipmentsByOrderId[$order_id] = $shipment;
            } catch (Exception $ex) {
                $errorMessage = "Order {$order_id} could not be exported to Frama because: {$ex->getMessage()}";
                $this->errors[$order_id] = $errorMessage;
                WCFR_Log::add($this->errors[$order_id]);
                continue;
            }
        }

        try {
            $api = $this->init_api();
            $postData = ["shipments" => $shipments];
            $resultArray = $api->export_shipments($postData);
        }
        catch (Exception $e) {
            WCFR_Log::add("Error creating shipments: {$e->getMessage()}");
            if (isset($postData)) {
                WCFR_Log::add("The json sent was: ".json_encode($postData));
            }
            throw new Exception("Error creating shipments: {$e->getMessage()}.");
        }

        WCFR_Log::add("Saving shipment ids");
        foreach ($shipmentsByOrderId as $order_id => $shipment) {
            if (isset($this->errors[$order_id])) {
                continue;
            }
            //write_log($resultArray);
            if (is_array($resultArray['body']['shipments'])) {
                $resultShipmentsArray = $resultArray['body']['shipments'];
                foreach ($resultShipmentsArray as $resultShipment) {
                    if ($order_id==$resultShipment['referenceId']) {
                        $order = WCX::get_order($order_id);
                        $shipmentMeta = ['shipment' => $shipment, 'id' => $resultShipment['id']];
                        $this->saveShipmentData($order, $shipmentMeta);
                        $this->success[$order_id] = $shipmentMeta['id'];
                    }
                }
            }
            else {
                WCFR_Log::add("Json response does not contain shipments in the body: ".print_r($resultArray,true));
            }
        }

        if (! empty($this->success) ) {
            $return["success"]     = sprintf(
                __("%s shipments successfully exported to Frama", "frama-woocommerce"),
                count($this->success)
            );
            $return["success_ids"] = $this->success;

            WCFR_Log::add($return["success"]);
            WCFR_Log::add("ids: " . implode(", ", $return["success_ids"]));
        }
        else {
            throw new Exception('No shipments exported');
        }

        return $return;
    }

    /**
     * @param array       $shipment_ids
     * @param array       $order_ids
     * @param int         $offset
     * @param string|null $displayOverride - Overrides display setting.
     *
     * @return array
     * @throws Exception
     */
    public function downloadOrGetUrlOfLabels(
        array $shipment_ids,
        array $order_ids = [],
        int $offset = 0
    ): array
    {
        $return = [];

        if (empty($shipment_ids)) WCFR_Log::add("downloadOrGetUrlOfLabels(): empty $shipment_ids");
        //else WCFR_Log::add("downloadOrGetUrlOfLabels(): Order IDs: " . print_r($order_ids, true));

        try {
            $api = $this->init_api();
            $openInNewTab = true; //WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_DOWNLOAD_DISPLAY) === "display"; // "display" means "open in new tab", "download" is the other option
            $api->getShipmentLabels($shipment_ids, $order_ids, $offset, $openInNewTab);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $return;
    }

    /**
     * @param array       $order_ids
     * @param int         $offset
     * @param string|null $display
     *
     * @return array
     * @throws Exception
     */
    public function getOrderLabels(array $order_ids, int $offset = 0): array
    {
        $shipment_ids = $this->getShipmentIds($order_ids, []);
        //WCFR_Log::add(print_r($order_ids, true)." > ordergetShipmentIds > ".print_r($shipment_ids, true));

        if (empty($shipment_ids)) {
            WCFR_Log::add(" *** Failed label request(not exported yet) ***");

            throw new Exception(__(
                "The selected orders have not been exported to Frama yet! ",
                "frama-woocommerce"
            ));
        }

        return $this->downloadOrGetUrlOfLabels(
            $shipment_ids,
            $order_ids,
            $offset
        );
    }

    /**
     * @param $order_ids
     */
    public function modal_dialog($order_ids, $dialog): void
    {
        // check for JSON
        if (is_string($order_ids) && strpos($order_ids, "[") !== false) {
            $order_ids = json_decode(stripslashes($order_ids));
        }

        // cast as array for single exports
        $order_ids = (array) $order_ids;
        die();
    }

    /**
     * @param $request
     * @param $result
     */
    public function modal_success_page($request, $result)
    {
        require("views/html-modal-result-page.php");
        die();
    }

    /**
     * @return WCFR_API
     * @throws Exception
     */
    public function init_api(): WCFR_API
    {
        $key = $this->getSetting(WCFRAMA_Settings::SETTING_API_KEY);

        if (! ($key)) {
            throw new ErrorException(__("No API key found in Frama settings", "frama-woocommerce"));
        }

        return new WCFR_API($key);
    }

    /**
     * @throws JsonException
     */
    public function prepareShipmentData($order_id, $order, ?array $options = []): array {
        WCFR_Log::add("Creating shipment data for order-id ".$order_id);
        $deliveryOptions = WCFRAMA_Admin::getDeliveryOptionsFromOrder($order);
        $pickupLocation = WCX_Order::get_meta($order,WCFRAMA_Admin::META_PICKUP_LOCATION);
        $shippingCountry = WCX_Order::get_prop($order, 'shipping_country');
        $extraOptions    = WCX_Order::get_meta($order, WCFRAMA_Admin::META_SHIPMENT_OPTIONS_EXTRA);

        $shipping_name =
            method_exists($order, "get_formatted_shipping_full_name") ? $order->get_formatted_shipping_full_name()
                : trim($order->get_shipping_first_name() . " " . $order->get_shipping_last_name());
        $email = WCX_Order::get_prop($order, "billing_email");


        switch (WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_SELECTED_CARRIER)) {
            case BPostConsignment::CARRIER_NAME:
                $carrierId = BPostConsignment::CARRIER_ID;
                break;
            case UPSConsignment::CARRIER_NAME:
                $carrierId = UPSConsignment::CARRIER_ID;
                break;
            default:
                $carrierId = PostNLConsignment::CARRIER_ID;
        }

        $labelText = $deliveryOptions->getShipmentOptions()->getLabelDescription();
        if (!isset($labelText)) $labelText = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_LABEL_DESCRIPTION); // get value from setting if not changed yet
        if (!isset($labelText)) $labelText = "Order: " . $this->order->get_id(); // default value
        $labelText = $this->getFormattedLabelDescription($order, $labelText);
        //WCFR_Log::add("Order ".$order->get_id()." label reference: ".$labelText);

        $shipment_data = [
            "referenceId" => $order_id,
            "carrier" => $carrierId,
            "labelText" => $labelText,
        ];
        if (WCFRAMA()->setting_collection->isEnabled(WCFRAMA_Settings::SETTING_TRACK_TRACE_EMAIL_VIA_API)) {
            $shipment_data["trackTraceEmail"] = $email;
        }
        $deliveryEmail = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_DELIVERY_EMAIL_VIA_API);
        if (!empty($deliveryEmail)) {
            $shipment_data["deliveryEmail"] = $deliveryEmail;
        }

        $address = [
            "typeId"  => 1, // 1 = Receiver, 3 = Pick up
            "company" => (string) WCX_Order::get_prop($order, "shipping_company"),
            "contact" => $shipping_name,
            "numberSuffix" => (string) WCX_Order::get_meta($order, "_shipping_house_number_suffix"),
            "extraAddress" => "",
            "zipCode" => (string) WCX_Order::get_prop($order, "shipping_postcode"),
            "email"   => $email,
            "phone" => (string) WCX_Order::get_prop($order, "shipping_phone"),
            "city" => (string) WCX_Order::get_prop($order, "shipping_city"),
            "countryCode" => $shippingCountry,
        ];
        $street = (string) WCX_Order::get_meta($order, "_shipping_street_name");
        if (empty($street)) {
            $street = (string)WCX_Order::get_prop($order, "shipping_address_1");
        }
        $address["street"] = $street;
        $houseNumber = (string) WCX_Order::get_meta($order, "_shipping_house_number");
        if (empty($houseNumber)) {
            $houseNumber = (string)WCX_Order::get_prop($order, "shipping_address_2");
        }
        if (!empty($houseNumber)) {
            $address["number"] = $houseNumber;
        }

        $shipment_data['addresses'] = [$address];
        if (!empty($pickupLocation)) {
            //WCFR_Log::add(WCFRAMA_Admin::META_PICKUP_LOCATION.": ".print_r($pickupLocation, true));
            $address = [
                "typeId"  => 3, // 1 = Receiver, 3 = Pick up
                "company" => $pickupLocation['name']??'',
                "street" => $pickupLocation['street']??'',
                "number" => $pickupLocation['number']??'',
                "numberSuffix" => $pickupLocation['numberSuffix']??'',
                "zipCode" => $pickupLocation['zipcode']??'',
                "city" => $pickupLocation['city']??'',
                "countryCode" => $pickupLocation['countryCode']??'',
            ];
            $shipment_data['addresses'][] = $address; //add the pickup location address
            //WCFR_Log::add("addresses: ".print_r($shipment_data['addresses'], true));
        }

        $packageTypeInt = WCFR_Data::getPackageTypeId($deliveryOptions->getPackageType());
        $ageCheck = (bool)$deliveryOptions->getShipmentOptions()->hasAgeCheck();
        $weight = floatval($extraOptions['weight']);
        if (empty($weight)) {
            if ($shippingCountry=='NL' && $packageTypeInt==AbstractConsignment::PACKAGE_TYPE_PACKAGE) {
                $weight = 10000;
            }
            else $weight = 2000;
        } else $weight = self::convertWeightToGrams($weight);
        $properties = [
            "package" => $packageTypeInt,// 1 = package, 2 = mailbox
            "weight" => $weight,
            "noNeighbour" => $ageCheck || $deliveryOptions->getShipmentOptions()->hasOnlyRecipient(),
            "signature" => $ageCheck || $deliveryOptions->getShipmentOptions()->hasSignature(),
            "ageCheck" => $ageCheck,
        ];
        if ($deliveryOptions->getShipmentOptions()->getInsurance()) {
            $properties["insurance"] = $deliveryOptions->getShipmentOptions()->getInsurance(); //100, 250, multiple of 500 till 5000
        }
        if (!empty($pickupLocation)) {
            $properties["delivery"] = 8;
        }
        $shipment_data["properties"] = $properties;

        return $shipment_data;
    }

    /**
     * @param WC_Order $order
     *
     * @return mixed|void
     * @throws Exception
     */
    public static function getRecipientFromOrder(WC_Order $order)
    {
        $isUsingFramaFields = WCX_Order::has_meta($order, "_billing_street_name")
                                 && WCX_Order::has_meta($order, "_billing_house_number");

        $shipping_name =
            method_exists($order, "get_formatted_shipping_full_name") ? $order->get_formatted_shipping_full_name()
                : trim($order->get_shipping_first_name() . " " . $order->get_shipping_last_name());


        $connectEmail = WCFRAMA()->setting_collection->isEnabled(WCFRAMA_Settings::SETTING_CONNECT_EMAIL);
        $connectPhone = WCFRAMA()->setting_collection->isEnabled(WCFRAMA_Settings::SETTING_CONNECT_PHONE);

        $address = [
            "cc"                     => (string) WCX_Order::get_prop($order, "shipping_country"),
            "city"                   => (string) WCX_Order::get_prop($order, "shipping_city"),
            "person"                 => $shipping_name,
            "company"                => (string) WCX_Order::get_prop($order, "shipping_company"),
            "email"                  => $connectEmail ? WCX_Order::get_prop($order, "billing_email") : "",
            "phone"                  => $connectPhone ? WCX_Order::get_prop($order, "billing_phone") : "",
            "street_additional_info" => WCX_Order::get_prop($order, "shipping_address_2"),
        ];

        $shipping_country = WCX_Order::get_prop($order, "shipping_country");
        if ($shipping_country === "NL") {
            // use billing address if old "pakjegemak" (1.5.6 and older)
            $pgAddress = WCX_Order::get_meta($order, WCFRAMA_Admin::META_PGADDRESS);

            if ($pgAddress) {
                $billing_name = method_exists($order, "get_formatted_billing_full_name")
                    ? $order->get_formatted_billing_full_name()
                    : trim(
                        $order->get_billing_first_name() . " " . $order->get_billing_last_name()
                    );
                $address_intl = [
                    "city"        => (string) WCX_Order::get_prop($order, "billing_city"),
                    "person"      => $billing_name,
                    "company"     => (string) WCX_Order::get_prop($order, "billing_company"),
                    "postal_code" => (string) WCX_Order::get_prop($order, "billing_postcode"),
                ];

                if ($isUsingFramaFields) {
                    $address_intl["street"]        = (string) WCX_Order::get_meta($order, "_billing_street_name");
                    $address_intl["number"]        = (string) WCX_Order::get_meta($order, "_billing_house_number");
                    $address_intl["number_suffix"] =
                        (string) WCX_Order::get_meta($order, "_billing_house_number_suffix");
                } else {
                    // Split the address line 1 into three parts
                    preg_match(
                        WCFR_NL_Postcode_Fields::SPLIT_STREET_REGEX,
                        WCX_Order::get_prop($order, "billing_address_1"),
                        $address_parts
                    );
                    $address_intl["street"]                 = (string) $address_parts["street"];
                    $address_intl["number"]                 = (string) $address_parts["number"];
                    $address_intl["number_suffix"]          =
                        array_key_exists("number_suffix", $address_parts) // optional
                            ? (string) $address_parts["number_suffix"] : "";
                    $address_intl["street_additional_info"] = WCX_Order::get_prop($order, "billing_address_2");
                }
            } else {
                $address_intl = [
                    "postal_code" => (string) WCX_Order::get_prop($order, "shipping_postcode"),
                ];
                // If not using old fields
                if ($isUsingFramaFields) {
                    $address_intl["street"]        = (string) WCX_Order::get_meta($order, "_shipping_street_name");
                    $address_intl["number"]        = (string) WCX_Order::get_meta($order, "_shipping_house_number");
                    $address_intl["number_suffix"] =
                        (string) WCX_Order::get_meta($order, "_shipping_house_number_suffix");
                } else {
                    // Split the address line 1 into three parts
                    preg_match(
                        WCFR_NL_Postcode_Fields::SPLIT_STREET_REGEX,
                        WCX_Order::get_prop($order, "shipping_address_1"),
                        $address_parts
                    );

                    $address_intl["street"]        = (string) $address_parts["street"];
                    $address_intl["number"]        = (string) $address_parts["number"];
                    $address_intl["number_suffix"] = (string) $address_parts["extension"] ?: "";

                    if (! $address_intl["number_suffix"]) {
                        if (preg_match(self::SUFFIX_CHECK_REG, $address["street_additional_info"])) {
                            $address_intl["number_suffix"]     = $address["street_additional_info"];
                            $address["street_additional_info"] = "";
                        }
                    }
                }
            }
        } else {
            $address_intl = [
                "postal_code"            => (string) WCX_Order::get_prop($order, "shipping_postcode"),
                "street"                 => (string) WCX_Order::get_prop($order, "shipping_address_1"),
                "street_additional_info" => (string) WCX_Order::get_prop($order, "shipping_address_2"),
                "region"                 => (string) WCX_Order::get_prop($order, "shipping_state"),
            ];
        }

        $address = array_merge($address, $address_intl);

        return apply_filters("wc_frama_recipient", $address, $order);
    }

    /**
     * @param int $order_id
     * @param string $track_trace
     *
     * @internal param $shipment_ids
     */
    public static function addTrackTraceNoteToOrder(int $order_id, string $track_trace): void
    {
        if (! WCFRAMA()->setting_collection->isEnabled(WCFRAMA_Settings::SETTING_BARCODE_IN_NOTE)) {
            return;
        }

        $prefix_message = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_BARCODE_IN_NOTE_TITLE);

        // Select the barcode text of the Frama settings
        $prefix_message = $prefix_message ? $prefix_message . " " : "";

        $order = WCX::get_order($order_id);
        $order->add_order_note($prefix_message . $track_trace);
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    private function getSetting(string $name)
    {
        return WCFRAMA()->setting_collection->getByName($name);
    }

    /**
     * @param array $order_ids
     * @param array $args
     *
     * @return array
     */
    public function getShipmentIds(array $order_ids, array $args): array
    {
        $shipment_ids = [];

        foreach ($order_ids as $order_id) {
            $order           = WCX::get_order($order_id);
            $order_shipments = WCX_Order::get_meta($order, WCFRAMA_Admin::META_SHIPMENTS);

            if (empty($order_shipments)) {
                continue;
            }

            foreach ($order_shipments as $shipment) {
                $shipment_ids[] = $shipment['id'];
            }
        }
        return $shipment_ids;
    }

    /**
     * @param WC_Order $order
     * @param array    $shipment
     *
     * @return void
     * @throws Exception
     */
    public function saveShipmentData(WC_Order $order, array $shipment): void
    {
        if (empty($shipment)) {
            throw new Exception("save_shipment_data requires a valid \$shipment.");
        }

        $new_shipments = [$shipment["id"] => $shipment];

        WCX_Order::update_meta_data($order, WCFRAMA_Admin::META_SHIPMENTS, $new_shipments);
    }

    /**
     * @param WC_Order $order
     * @param string   $shippingMethodId
     *
     * @return int|null
     * @throws Exception
     */
    public function getOrderShippingClass(WC_Order $order, string $shippingMethodId = ''): ?int
    {
        if (empty($shippingMethodId)) {
            $orderShippingMethods = $order->get_items('shipping');

            if (! empty($orderShippingMethods)) {
                // we're taking the first (we're not handling multiple shipping methods as of yet)
                $orderShippingMethod = array_shift($orderShippingMethods);
                $shippingMethodId    = $orderShippingMethod['method_id'];
            } else {
                return null;
            }
        }

        $shippingMethod = $this->getShippingMethod($shippingMethodId);

        if (empty($shippingMethod)) {
            return null;
        }

        // get shipping classes from order
        $foundShippingClasses = $this->find_order_shipping_classes($order);
        $highest_class = $this->getShippingClass($shippingMethod, $foundShippingClasses);

        return $highest_class;
    }

    /**
     * Determine appropriate package type for this order.
     *
     * @param WC_Order                            $order
     * @param AbstractDeliveryOptionsAdapter|null $deliveryOptions
     *
     * @return string
     * @throws Exception
     */
    public function getPackageTypeFromOrder(WC_Order $order, AbstractDeliveryOptionsAdapter $deliveryOptions = null): string
    {
        $packageTypeFromDeliveryOptions = $deliveryOptions
            ? $deliveryOptions->getPackageType()
            : null;

        if ($packageTypeFromDeliveryOptions) {
            return $packageTypeFromDeliveryOptions;
        }

        $packageType = AbstractConsignment::DEFAULT_PACKAGE_TYPE_NAME;

        // get shipping methods from order
        $orderShippingMethods = $order->get_items('shipping');

        if (! empty($orderShippingMethods)) {
            // we're taking the first (we're not handling multiple shipping methods as of yet)
            $orderShippingMethod = array_shift($orderShippingMethods);
            $orderShippingMethod = $orderShippingMethod['method_id'];

            $orderShippingClass = WCX_Order::get_meta($order, WCFRAMA_Admin::META_HIGHEST_SHIPPING_CLASS);
            if (empty($orderShippingClass)) {
                $orderShippingClass = $this->getOrderShippingClass($order, $orderShippingMethod);
            }

            $packageType = self::getPackageTypeFromShippingMethod(
                $orderShippingMethod,
                $orderShippingClass
            );
        }

        return $this->getAllowedPackageType($order, $packageType);
    }

    /**
     * @param $shippingMethod
     * @param $shippingClass
     *
     * @return string
     */
    public static function getPackageTypeFromShippingMethod($shippingMethod, $shippingClass): string
    {
        $packageType           = AbstractConsignment::PACKAGE_TYPE_PACKAGE_NAME;
        $shippingMethodIdClass = $shippingMethod;

        if (Str::startsWith($shippingMethod, 'table_rate:') && class_exists('WC_Table_Rate_Shipping')) {
            // Automattic / WooCommerce table rate
            // use full method = method_id:instance_id:rate_id
            $shippingMethodId = $shippingMethod;
        } else {
            // non table rates
            if (Str::contains($shippingMethodIdClass, ':')) {
                // means we have method_id:instance_id
                $shippingMethod   = explode(':', $shippingMethod);
                $shippingMethodId = $shippingMethod[0];
            } else {
                $shippingMethodId = $shippingMethod;
            }

            // add class if we have one
            if (! empty($shippingClass)) {
                $shippingMethodIdClass = "{$shippingMethodId}:{$shippingClass}";
            }
        }

        $packageTypes = WCFRAMA()->setting_collection->getByName(WCFRAMA_Settings::SETTING_SHIPPING_METHODS_PACKAGE_TYPES);
        foreach ($packageTypes as $packageTypeKey => $packageTypeShippingMethods) {
            if (self::isActiveMethod(
                $shippingMethodId,
                $packageTypeShippingMethods,
                $shippingMethodIdClass,
                $shippingClass
            )) {
                $packageType = $packageTypeKey;
                break;
            }
        }

        return self::getPackageTypeAsString($packageType);
    }

    /**
     * @param string|null $packageType
     *
     * @return string
     */
    public static function getPackageTypeHuman(?string $packageType): string
    {
        if ($packageType) {
            $packageType = WCFR_Data::getPackageTypeHuman($packageType);
        }

        return $packageType ?? __("Unknown", "frama-woocommerce");
    }

    /**
     * Will convert any package type to a valid string package type.
     *
     * @param mixed $packageType
     *
     * @return string
     */
    public static function getPackageTypeAsString($packageType): string
    {
        if (is_numeric($packageType)) {
            $packageType = WCFR_Data::getPackageTypeName($packageType);
        }

        if (! is_string($packageType) || ! in_array($packageType, WCFR_Data::getPackageTypes())) {
            // Log data when this occurs but don't actually throw an exception.
            $type = gettype($packageType);
            WCFR_Log::add(new Exception("Tried to convert invalid value to package type: $packageType ($type)"));

            $packageType = null;
        }

        return $packageType ?? AbstractConsignment::DEFAULT_PACKAGE_TYPE_NAME;
    }

    /**
     * @param WC_Order $order
     * @param string   $packageType
     *
     * @return string
     *
     * @throws Exception
     */
    public function getAllowedPackageType(WC_Order $order, string $packageType): string
    {
        $shippingCountry      = WCX_Order::get_prop($order, "shipping_country");
        $isMailbox            = AbstractConsignment::PACKAGE_TYPE_MAILBOX_NAME === $packageType;
        //$isDigitalStamp       = AbstractConsignment::PACKAGE_TYPE_DIGITAL_STAMP_NAME === $packageType;
        $isDefaultPackageType = AbstractConsignment::CC_NL !== $shippingCountry && ($isMailbox); //|| $isDigitalStamp);

        if ($isDefaultPackageType) {
            $packageType = AbstractConsignment::DEFAULT_PACKAGE_TYPE_NAME;
        }

        return $packageType;
    }

    /**
     * @param $errors
     *
     * @return mixed|string
     */
    public function parse_errors($errors)
    {
        $parsed_errors = [];

        foreach ($errors as $key => $error) {
            // check if we have an order_id
            if ($key > 10) {
                $parsed_errors[] = sprintf(
                    "<strong>%s %s:</strong> %s",
                    __("Order", "frama-woocommerce"),
                    $key,
                    $error
                );
            } else {
                $parsed_errors[] = $error;
            }
        }

        if (count($parsed_errors) == 1) {
            $html = array_shift($parsed_errors);
        } else {
            foreach ($parsed_errors as &$parsed_error) {
                $parsed_error = "<li>{$parsed_error}</li>";
            }
            $html = sprintf("<ul>%s</ul>", implode("\n", $parsed_errors));
        }

        return $html;
    }

    public function getShipmentStatusName($status_code)
    {
        $shipment_statuses = [
            1  => __("pending - concept", "frama-woocommerce"),
            2  => __("pending - registered", "frama-woocommerce"),
            3  => __("enroute - handed to carrier", "frama-woocommerce"),
            4  => __("enroute - sorting", "frama-woocommerce"),
            5  => __("enroute - distribution", "frama-woocommerce"),
            6  => __("enroute - customs", "frama-woocommerce"),
            7  => __("delivered - at recipient", "frama-woocommerce"),
            8  => __("delivered - ready for pickup", "frama-woocommerce"),
            9  => __("delivered - package picked up", "frama-woocommerce"),
            12 => __("printed - letter", "frama-woocommerce"),
            14 => __("printed - digital stamp", "frama-woocommerce"),
            30 => __("inactive - concept", "frama-woocommerce"),
            31 => __("inactive - registered", "frama-woocommerce"),
            32 => __("inactive - enroute - handed to carrier", "frama-woocommerce"),
            33 => __("inactive - enroute - sorting", "frama-woocommerce"),
            34 => __("inactive - enroute - distribution", "frama-woocommerce"),
            35 => __("inactive - enroute - customs", "frama-woocommerce"),
            36 => __("inactive - delivered - at recipient", "frama-woocommerce"),
            37 => __("inactive - delivered - ready for pickup", "frama-woocommerce"),
            38 => __("inactive - delivered - package picked up", "frama-woocommerce"),
            99 => __("inactive - unknown", "frama-woocommerce"),
        ];

        if (isset($shipment_statuses[$status_code])) {
            return $shipment_statuses[$status_code];
        } else {
            return __("Unknown status", "frama-woocommerce");
        }
    }

    /**
     * Returns the weight in grams.
     *
     * @param int|float $weight
     *
     * @return int
     */
    public static function convertWeightToGrams($weight): int
    {
        $weightUnit  = get_option('woocommerce_weight_unit');
        $floatWeight = (float) $weight;

        switch ($weightUnit) {
            case 'kg':
                $weight = $floatWeight * 1000;
                break;
            case 'lbs':
                $weight = $floatWeight / 0.45359237;
                break;
            case 'oz':
                $weight = $floatWeight / 0.0283495231;
                break;
            default:
                $weight = $floatWeight;
                break;
        }

        return (int) ceil($weight);
    }

    /**
     * @return array
     */
    public static function getDigitalStampRangeOptions(): array
    {
        $options = [];

        foreach (WCFR_Data::getDigitalStampRanges() as $key => $tierRange) {
            $options[$tierRange['average']] = $tierRange['min'] . " - " . $tierRange['max'] . " gram";
        }

        return $options;
    }

    /**
     * @param string $chosenMethod
     *
     * @return bool|WC_Shipping_Method|null
     */
    public static function getShippingMethod(string $chosenMethod)
    {
        if (version_compare(WOOCOMMERCE_VERSION, "2.6", "<") || $chosenMethod === WCFR_Shipping_Methods::LEGACY_FLAT_RATE) {
            return self::getLegacyShippingMethod($chosenMethod);
        }

        [$methodSlug, $methodInstance] = WCFR_Checkout::splitShippingMethodString($chosenMethod);

        $isDisallowedShippingMethod = in_array($methodSlug, self::DISALLOWED_SHIPPING_METHODS);
        $isManualOrder              = empty($methodInstance);

        if ($isDisallowedShippingMethod || $isManualOrder) {
            return null;
        }

        return WC_Shipping_Zones::get_shipping_method($methodInstance) ?? null;
    }

    /**
     * @param string $chosen_method
     *
     * @return null|WC_Shipping_Method
     */
    private static function getLegacyShippingMethod(string $chosen_method): ?WC_Shipping_Method
    {
        // only for flat rate or legacy flat rate
        if (! in_array(
            $chosen_method,
            [
                WCFR_Shipping_Methods::FLAT_RATE,
                WCFR_Shipping_Methods::LEGACY_FLAT_RATE,
            ]
        )) {
            return null;
        }

        $shipping_methods = WC()->shipping()->load_shipping_methods();

        if (! isset($shipping_methods[$chosen_method])) {
            return null;
        }

        return $shipping_methods[$chosen_method];
    }

    /**
     * @param $shipping_method
     * @param $found_shipping_classes
     *
     * @return int|null
     */
    public function getShippingClass($shipping_method, $found_shipping_classes): ?int
    {
        // get most expensive class
        // adapted from $shipping_method->calculate_shipping()
        $highest_class_cost = 0;
        $highest_class      = null;
        foreach ($found_shipping_classes as $shipping_class => $products) {
            // Also handles BW compatibility when slugs were used instead of ids
            $shipping_class_term    = get_term_by("slug", $shipping_class, "product_shipping_class");
            $shipping_class_term_id = "";

            if ($shipping_class_term != null) {
                $shipping_class_term_id = $shipping_class_term->term_id;
            }

            $class_cost_string = $shipping_class_term && $shipping_class_term_id ? $shipping_method->get_option(
                "class_cost_" . $shipping_class_term_id,
                $shipping_method->get_option("class_cost_" . $shipping_class, "")
            ) : $shipping_method->get_option("no_class_cost", "");

            if ($class_cost_string === "") {
                continue;
            }

            $class_cost = $this->wc_flat_rate_evaluate_cost(
                $class_cost_string,
                [
                    "qty"  => array_sum(wp_list_pluck($products, "quantity")),
                    "cost" => array_sum(wp_list_pluck($products, "line_total")),
                ],
                $shipping_method
            );
            if ($class_cost > $highest_class_cost && ! empty($shipping_class_term_id)) {
                $highest_class_cost = $class_cost;
                $highest_class      = $shipping_class_term->term_id;
            }
        }

        return $highest_class;
    }

    /**
     * Adapted from WC_Shipping_Flat_Rate - Protected method
     * Evaluate a cost from a sum/string.
     *
     * @param string $sum
     * @param array  $args
     * @param        $flat_rate_method
     *
     * @return string
     */
    public function wc_flat_rate_evaluate_cost(string $sum, array $args, $flat_rate_method)
    {
        if (version_compare(WOOCOMMERCE_VERSION, "2.6", ">=")) {
            include_once(WC()->plugin_path() . "/includes/libraries/class-wc-eval-math.php");
        } else {
            include_once(WC()->plugin_path() . "/includes/shipping/flat-rate/includes/class-wc-eval-math.php");
        }

        // Allow 3rd parties to process shipping cost arguments
        $args           = apply_filters("woocommerce_evaluate_shipping_cost_args", $args, $sum, $flat_rate_method);
        $locale         = localeconv();
        $decimals       = [
            wc_get_price_decimal_separator(),
            $locale["decimal_point"],
            $locale["mon_decimal_point"],
            ",",
        ];
        $this->fee_cost = $args["cost"];

        // Expand shortcodes
        add_shortcode("fee", [$this, "wc_flat_rate_fee"]);

        $sum = do_shortcode(
            str_replace(
                ["[qty]", "[cost]"],
                [$args["qty"], $args["cost"]],
                $sum
            )
        );

        remove_shortcode("fee");

        // Remove whitespace from string
        $sum = preg_replace("/\s+/", "", $sum);

        // Remove locale from string
        $sum = str_replace($decimals, ".", $sum);

        // Trim invalid start/end characters
        $sum = rtrim(ltrim($sum, "\t\n\r\0\x0B+*/"), "\t\n\r\0\x0B+-*/");

        // Do the math
        return $sum ? WC_Eval_Math::evaluate($sum) : 0;
    }

    /**
     * Adapted from WC_Shipping_Flat_Rate - Protected method
     * Work out fee (shortcode).
     *
     * @param array $atts
     *
     * @return string
     */
    public function wc_flat_rate_fee($atts)
    {
        $atts = shortcode_atts(
            [
                "percent" => "",
                "min_fee" => "",
                "max_fee" => "",
            ],
            $atts
        );

        $calculated_fee = 0;

        if ($atts["percent"]) {
            $calculated_fee = $this->fee_cost * (floatval($atts["percent"]) / 100);
        }

        if ($atts["min_fee"] && $calculated_fee < $atts["min_fee"]) {
            $calculated_fee = $atts["min_fee"];
        }

        if ($atts["max_fee"] && $calculated_fee > $atts["max_fee"]) {
            $calculated_fee = $atts["max_fee"];
        }

        return $calculated_fee;
    }

    /**
     * Filter out orders shipping to country codes that are not in the allowed list.
     *
     * @param array $order_ids
     *
     * @return mixed
     * @throws Exception
     */
    public function filterOrderDestinations(array $order_ids): array
    {
        foreach ($order_ids as $key => $order_id) {
            $order            = WCX::get_order($order_id);
            $shipping_country = WCX_Order::get_prop($order, "shipping_country");

            if (! WCFR_Country_Codes::isAllowedDestination($shipping_country)) {
                unset($order_ids[$key]);
            }
        }

        return $order_ids;
    }

    /**
     * @param $shipping_method_id
     * @param $package_type_shipping_methods
     * @param $shipping_method_id_class
     * @param $shipping_class
     *
     * @return bool
     */
    private static function isActiveMethod(
        $shipping_method_id,
        $package_type_shipping_methods,
        $shipping_method_id_class,
        $shipping_class
    ) {
        //support WooCommerce flat rate
        // check if we have a match with the predefined methods
        if (in_array($shipping_method_id, $package_type_shipping_methods)) {
            return true;
        }

        if (in_array($shipping_method_id_class, $package_type_shipping_methods)) {
            return true;
        }

        // fallback to bare method (without class) (if bare method also defined in settings)
        if (! empty($shipping_method_id_class)
            && in_array(
                $shipping_method_id_class,
                $package_type_shipping_methods
            )) {
            return true;
        }

        // support WooCommerce Table Rate Shipping by WooCommerce
        if (! empty($shipping_class) && in_array($shipping_class, $package_type_shipping_methods)) {
            return true;
        }

        // support WooCommerce Table Rate Shipping by Bolder Elements
        $newShippingClass = str_replace(":", "_", $shipping_class);
        if (! empty($shipping_class) && in_array($newShippingClass, $package_type_shipping_methods)) {
            return true;
        }

        return false;
    }

    /**
     * @param array $order_ids
     * @param array $shipment_ids
     * @param int   $offset
     *
     * @return array
     * @throws Exception
     */
    private function printLabels(array $order_ids, array $shipment_ids, int $offset): array
    {
        if (! empty($shipment_ids)) {
            $return = $this->downloadOrGetUrlOfLabels(
                $shipment_ids,
                $order_ids,
                $offset
            );
        } else {
            $order_ids = $this->filterOrderDestinations($order_ids);
            $return    = $this->getOrderLabels($order_ids, $offset);
        }

        return $return;
    }

    /**
     * @param $order_ids
     * @param $offset
     * @param $print
     *
     * @return void
     * @throws ApiException
     * @throws ErrorException
     * @throws Exception
     */
    private function addShipments($order_ids, $offset, $print): void
    {
        $order_ids = $this->filterOrderDestinations($order_ids);

        if (empty($order_ids)) {
            $this->errors[] =
                __(
                    "The order(s) you have selected have invalid shipping countries.",
                    "frama-woocommerce"
                );

            return;
        }

        // if we're going to print directly, we need to process the orders first, regardless of the settings
        $process = $print === "yes";
        $return  = $this->add_shipments($order_ids);

        // When adding shipments, store $return for use in admin_notice
        // This way we can refresh the page (JS) to show all new buttons
        if ($print === "no" || $print === "after_reload") {
            update_option("wcframa_admin_notices", $return);
            if ($print === "after_reload") {
                $print_queue = [
                    "order_ids"    => $order_ids,
                    "shipment_ids" => $return["success_ids"],
                    "offset"       => isset($offset) && is_numeric($offset) ? $offset % 4 : 0,
                ];
                update_option("wcframa_print_queue", $print_queue);
            }
        }

    }

    /**
     * Save created track & trace information as meta data to the corresponding order(s).
     *
     * @param array              $labels
     * @param array              $order_ids
     */
    public static function saveTrackTracesToOrders(array $labels, array $order_ids): void
    {
        foreach ($order_ids as $order_id) {
            $order = WCX::get_order($order_id);
            $order_shipments_data = WCX_Order::get_meta($order, WCFRAMA_Admin::META_SHIPMENTS) ?? array();
            $new_shipments_data = [];
            foreach ($order_shipments_data as $shipment_id => $shipment) {
                foreach ($labels as $label) {
                    if ($label['id'] == $shipment_id) {
                        $shipment['barcode'] = $label['barcode'];
                        $shipment['trackTraceUrl'] = $label['trackTraceUrl'];
                        $new_shipments_data[$shipment["id"]] = $shipment;
                        WCFR_Export::addTrackTraceNoteToOrder($order_id, $label['trackTraceUrl']);
                    }
                }
            }
            //WCFR_Log::add("saving Track&Trace To Order ".$order->get_id().': '.print_r($new_shipments_data, true));

            WCX_Order::update_meta_data($order, WCFRAMA_Admin::META_SHIPMENTS, $new_shipments_data);
        }
    }

    public function getShipmentData(int $order_id)
    {
        $order = WCX::get_order($order_id);
        return WCX_Order::get_meta($order, WCFRAMA_Admin::META_SHIPMENTS);
    }

    /**
     * Get the label description from OrderSettings and replace any variables in it.
     *
     * @param string|null $labelDescription
     * @return string
     */
    private function getFormattedLabelDescription($order, ?string $labelDescription): string
    {
        /** @var WC_Order $order */
        if (!isset($labelDescription)) return '';
        $productIds   = [];
        $productNames = [];
        $productSkus  = [];
        $productQtys  = [];
        $orderItemCount = 0;
        $orderQtyCount = 0;

        foreach ($order->get_items() as $item) {
            if (! method_exists($item, 'get_product')) {
                continue;
            }

            /** @var WC_Product $product */
            $product = $item->get_product();
            $sku     = $product->get_sku();
            $qty     = $item->get_quantity();

            $productIds[]   = $product->get_id();
            $productNames[] = $product->get_name();
            $productSkus[]  = empty($sku) ? '–' : $sku;
            $productQtys[]  = $qty;
            $orderItemCount++;
            $orderQtyCount += $qty;
        }
        $skusXQtys = [];
        $idsXQtys = [];
        $namesXQtys = [];
        for ($i = 0; $i < count($productQtys); $i++) {
            $skusXQtys[] = $productSkus[$i].'x'.$productQtys[$i];
            $idsXQtys[] = $productIds[$i].'x'.$productQtys[$i];
            $namesXQtys[] = $productNames[$i].'x'.$productQtys[$i];
        }

        $formattedLabelDescription = strtr(
            $labelDescription,
            [
                '[ORDER_NR]'         => $order->get_order_number(),
                '[ORDER_ITEM_COUNT]' => $orderItemCount,
                '[ORDER_QTY_SUM]'    => $orderQtyCount,
                '[PRODUCT_IDS]'      => implode(',', $productIds),
                '[PRODUCT_SKUS]'     => implode(',', $productSkus),
                '[PRODUCT_NAMES]'    => implode(',', $productNames),
                '[PRODUCT_QTYS]'     => implode(',', $productQtys),
                '[PRODUCT_SKUSxQTY]' => implode(' ', $skusXQtys),
                '[PRODUCT_IDSxQTY]'  => implode(' ', $idsXQtys),
                '[PRODUCT_NAMESxQTY]'=> implode(' ', $namesXQtys),
                '[CUSTOMER_NOTE]'    => $order->get_customer_note(),
            ]
        );

        if (strlen($formattedLabelDescription) > WCFR_Export::ORDER_DESCRIPTION_MAX_LENGTH) {
            return substr($formattedLabelDescription, 0, 42) . "***";
        }

        return $formattedLabelDescription;
    }
}

return new WCFR_Export();
