<?php

use FramaNL\Model\Consignment\BPostConsignment;
use FramaNL\Model\Consignment\PostNLConsignment;
use FramaNL\Model\Consignment\UPSConsignment;
use WPO\WC\Frama\Entity\SettingsFieldArguments;

if (! defined('ABSPATH')) {
    exit;
}

if (class_exists('WCFR_Settings_Data')) {
    return new WCFR_Settings_Data();
}

/**
 * This class contains all data for the admin settings screens created by the plugin.
 */
class WCFR_Settings_Data
{
    public const ENABLED  = "1";
    public const DISABLED = "0";

    public const DISPLAY_FOR_SELECTED_METHODS = "selected_methods";
    public const DISPLAY_FOR_ALL_METHODS      = "all_methods";

    /**
     * @var WCFR_Settings_Callbacks
     */
    private $callbacks;

    public function __construct()
    {
        $this->callbacks = require 'class-wcfr-settings-callbacks.php';

        // Create the Frama settings with the admin_init hook.
        add_action("admin_init", [$this, "create_all_settings"]);
    }

    /**
     * Create all settings sections.
     *
     * @throws Exception
     */
    public function create_all_settings(): void
    {
        $this->generate_settings(
            $this->get_sections_general(),
            WCFRAMA_Settings::SETTINGS_GENERAL
        );

        $this->generate_settings(
            $this->get_sections_export_defaults(),
            WCFRAMA_Settings::SETTINGS_EXPORT_DEFAULTS
        );

        $this->generate_settings(
            $this->get_sections_checkout(),
            WCFRAMA_Settings::SETTINGS_CHECKOUT
        );

        $this->generate_settings(
            $this->get_sections_carrier_postnl(),
            WCFRAMA_Settings::SETTINGS_EXPORT_DEFAULTS,
            true
        );
    }

    /**
     * @return array
     */
    public static function getTabs(): array
    {
        $array = [
            WCFRAMA_Settings::SETTINGS_GENERAL         => __("General", "frama-woocommerce"),
            WCFRAMA_Settings::SETTINGS_EXPORT_DEFAULTS => __("Shipment settings", "frama-woocommerce"),
            WCFRAMA_Settings::SETTINGS_CHECKOUT        => __("Checkout settings", "frama-woocommerce"),
        ];

        return $array;
    }

    /**
     * Generate settings sections and fields by the given $settingsArray.
     *
     * @param array  $settingsArray - Array of settings to loop through.
     * @param string $optionName    - Name to use in the identifier.
     * @param bool   $prefix        - Add the key of the top level settings as prefix before every setting or not.
     *
     * @throws \Exception
     */
    private function generate_settings(array $settingsArray, string $optionName, bool $prefix = false): void
    {
        $optionIdentifier = WCFRAMA_Settings::getOptionId($optionName);
        $defaults         = [];

        // Register settings.
        register_setting($optionIdentifier, $optionIdentifier, [$this->callbacks, 'validate']);

        foreach ($settingsArray as $name => $array) {
            foreach ($array as $section) {
                $sectionName = "{$name}_{$section["name"]}";

                add_settings_section(
                    $sectionName,
                    $section["label"],
                    /**
                     * Allows a description to be shown with a section title.
                     */
                    static function() use ($section) {
                        WCFR_Settings_Callbacks::renderSection($section);
                    },
                    $optionIdentifier
                );

                foreach ($section["settings"] as $setting) {
                    $namePrefix            = $prefix ? "{$name}_" : '';
                    $setting["id"]        = $prefix ? "{$name}_{$setting["name"]}" : $setting["name"];
                    $setting["option_id"] = $optionIdentifier;

                    $class = new SettingsFieldArguments($setting, "{$optionIdentifier}[{$namePrefix}", ']');

                    // Add the setting's default value to the defaults array.
                    $defaults[$setting["id"]] = $class->getDefault();

                    if (isset(get_option($optionIdentifier)[$class->getId()])) {
                        $class->setValue(get_option($optionIdentifier)[$class->getId()]);
                    }

                    // Default callback
                    $callback = static function() use ($class) {
                        WCFR_Settings_Callbacks::renderField($class);
                    };

                    // Pass the class to custom callbacks as well.
                    if (isset($setting['callback'])) {
                        $callback = static function () use ($setting, $class) {
                            call_user_func($setting["callback"], $class);
                        };
                    }

                    add_settings_field(
                        $setting["id"],
                        $setting["label"],
                        $callback,
                        $optionIdentifier,
                        $sectionName,
                        // If a custom callback is used, send the $setting as arguments. Otherwise use the created
                        // arguments from the class.
                        isset($setting["callback"]) ? $setting : $class->getArguments()
                    );
                }
            }
        }

        // Create option in wp_options with default settings if the option doesn't exist yet.
        if (false === get_option($optionIdentifier)) {
            add_option($optionIdentifier, $defaults);
        }

        // Merge any missing values into the settings
        update_option(
            $optionIdentifier,
            array_replace_recursive(
                $defaults,
                get_option($optionIdentifier)
            )
        );
    }

    /**
     * @return array
     */
    private function get_sections_general()
    {
        return [
            WCFRAMA_Settings::SETTINGS_GENERAL => [
                [
                    "name"     => "api",
                    "label"    => __("API settings", "frama-woocommerce"),
                    "settings" => $this->get_section_general_api(),
                ],
                [
                    "name"     => "general",
                    "label"    => __("General settings", "frama-woocommerce"),
                    "settings" => $this->get_section_general_general(),
                ],
                [
                    "name"     => "diagnostics",
                    "label"    => __("Diagnostic tools", "frama-woocommerce"),
                    "settings" => $this->get_section_general_diagnostics(),
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function get_sections_export_defaults()
    {
        return [
            WCFRAMA_Settings::SETTINGS_EXPORT_DEFAULTS => [
                [
                    "name"     => "main",
                    "label"    => __("Shipment settings", "frama-woocommerce"),
                    "settings" => $this->get_section_export_defaults_main(),
                ],
            ],
        ];
    }

    private function get_sections_checkout()
    {
        return [
            WCFRAMA_Settings::SETTINGS_CHECKOUT => [
                [
                    "name"     => "main",
                    "label"    => __("Checkout settings", "frama-woocommerce"),
                    "settings" => $this->get_section_checkout_main(),
                ],
                [
                    "name"      => "strings",
                    "label"     => __("Titles", "frama-woocommerce"),
                    "condition" => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_ENABLED,
                    "settings"  => $this->get_section_checkout_strings(),
                ],
            ],
        ];
    }

    /**
     * Get the array of PostNL sections and their settings to be added to WordPress.
     *
     * @return array
     */
    private function get_sections_carrier_postnl(): array
    {
        return [
            PostNLConsignment::CARRIER_NAME => [
                [
                    "name"        => "export_defaults",
                    "label"       => __("Default PostNL package settings", "frama-woocommerce"),
                    "description" => __(
                        "These settings will be applied to PostNL shipments you create in the backend.",
                        "frama-woocommerce"
                    ),
                    "settings"    => $this->get_section_carrier_postnl_export_defaults(),
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function get_section_general_api(): array
    {
        return [
            [
                "name"      => WCFRAMA_Settings::SETTING_API_KEY,
                "label"     => __("Key", "frama-woocommerce"),
                "help_text" => __("api key", "frama-woocommerce"),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_TRACK_TRACE_EMAIL_VIA_API,
                "label"     => __("Track&Trace mail via Frama", "frama-woocommerce"),
                "help_text" => __("Enable to have Frama email the track&trace info to the customer after creating a shipment", "frama-woocommerce"),
                "type"      => "toggle",
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_DELIVERY_EMAIL_VIA_API,
                "label"     => __("Delivery mail recipient", "frama-woocommerce"),
                "help_text" => __("Enter the email address to send notification of delivery to. Leave empty to disable", "frama-woocommerce"),
            ],
        ];
    }

    /**
     * @return array
     */
    private function get_section_general_general(): array
    {
        return [
//            [
//                "name"    => WCFRAMA_Settings::SETTING_DOWNLOAD_DISPLAY,
//                "label"   => __("Label display", "frama-woocommerce"),
//                "type"    => "select",
//                "options" => [
//                    "download" => __("Download PDF", "frama-woocommerce"),
//                    "display"  => __("Open PDF in a new tab", "frama-woocommerce"),
//                ],
//            ],
            [
                "name"    => WCFRAMA_Settings::SETTING_LABEL_FORMAT,
                "label"   => __("Label format", "frama-woocommerce"),
                "type"    => "select",
                "options" => [
                    "A4" => __("A4 - 4 positions", "frama-woocommerce"),
                    "A6" => __("Label 10x15cm (A6)", "frama-woocommerce"),
                    "DP" => __("Direct Print", "frama-woocommerce"),
                ],
            ],
            [
                "name"       => WCFRAMA_Settings::SETTING_ASK_FOR_PRINT_POSITION,
                "label"      => __("Ask for print start position", "frama-woocommerce"),
                "condition" => [
                    "parent_name"  => WCFRAMA_Settings::SETTING_LABEL_FORMAT,
                    "type"         => "disable",
                    "parent_value" => "A4",
                    "set_value"    => self::DISABLED,
                ],
                "type"       => "toggle",
                "help_text"  => __(
                    "This option enables you to continue printing where you left off last time",
                    "frama-woocommerce"
                ),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_TRACK_TRACE_EMAIL,
                "label"     => __("Track & Trace in email", "frama-woocommerce"),
                "type"      => "toggle",
                "help_text" => __(
                    "Add the Track & Trace code to emails to the customer.",
                    "frama-woocommerce"
                ),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_TRACK_TRACE_MY_ACCOUNT,
                "label"     => __("Track & Trace in My Account", "frama-woocommerce"),
                "type"      => "toggle",
                "help_text" => __("Show Track & Trace trace code and link in My Account.", "frama-woocommerce"),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_ORDER_STATUS_AUTOMATION,
                "label"     => __("Order status automation", "frama-woocommerce"),
                "type"      => "toggle",
                "help_text" => __(
                    "Automatically set order status to a predefined status, after a Frama label has successfully been created.",
                    "frama-woocommerce"
                ),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_AUTOMATIC_ORDER_STATUS,
                "condition" => WCFRAMA_Settings::SETTING_ORDER_STATUS_AUTOMATION,
                "class"     => ["wcframa__child"],
                "label"     => __("setting_automatic_order_status", "frama-woocommerce"),
                "type"      => "select",
                "options"   => WCFR_Settings_Callbacks::get_order_status_options(),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_BARCODE_IN_NOTE,
                "label"     => __("Place barcode inside note", "frama-woocommerce"),
                "type"      => "toggle",
                "help_text" => __("Place the barcode inside a note of the order", "frama-woocommerce"),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_BARCODE_IN_NOTE_TITLE,
                "condition" => WCFRAMA_Settings::SETTING_BARCODE_IN_NOTE,
                "class"     => ["wcframa__child"],
                "label"     => __("Title before the barcode", "frama-woocommerce"),
                "default"   => __("Track & trace code:", "frama-woocommerce"),
                "help_text" => __(
                    "You can change the text before the barcode inside an note",
                    "frama-woocommerce"
                ),
            ],
        ];
    }

    /**
     * @return array
     */
    private function get_section_general_diagnostics(): array
    {
        return [
            [
                "name"        => WCFRAMA_Settings::SETTING_ERROR_LOGGING,
                "label"       => __("Log API communication", "frama-woocommerce"),
                "type"        => "toggle",
                "description" => '<a href="' . esc_url_raw(
                        admin_url("admin.php?page=wc-status&tab=logs")
                    ) . '" target="_blank">' . __("View logs", "frama-woocommerce") . "</a> (wc-frama)",
            ],
        ];
    }

    /**
     * Export defaults specifically for postnl.
     *
     * @return array
     */
    private function get_section_carrier_postnl_export_defaults(): array
    {
        return [
            [
                "name"      => WCFRAMA_Settings::SETTING_CARRIER_DEFAULT_EXPORT_ONLY_RECIPIENT,
                "label"     => __("shipment_options_only_recipient", "frama-woocommerce"),
                "help_text" => __("shipment_options_only_recipient_help_text", "frama-woocommerce"),
                "type"      => "toggle",
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_CARRIER_DEFAULT_EXPORT_SIGNATURE,
                "label"     => __("shipment_options_signature", "frama-woocommerce"),
                "help_text" => __("shipment_options_signature_help_text", "frama-woocommerce"),
                "type"      => "toggle",
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_CARRIER_DEFAULT_EXPORT_AGE_CHECK,
                "label"     => __("shipment_options_age_check", "frama-woocommerce"),
                "type"      => "toggle",
                "help_text" => __("shipment_options_age_check_help_text", "frama-woocommerce"),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_CARRIER_DEFAULT_EXPORT_INSURED,
                "label"     => __("shipment_options_insured", "frama-woocommerce"),
                "help_text" => __("shipment_options_insured_help_text", "frama-woocommerce"),
                "type"      => "toggle",
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_CARRIER_DEFAULT_EXPORT_INSURED_FROM_PRICE,
                "condition" => WCFRAMA_Settings::SETTING_CARRIER_DEFAULT_EXPORT_INSURED,
                "label"     => __("shipment_options_insured_from_price", "frama-woocommerce"),
                "help_text" => __("shipment_options_insured_from_price_help_text", "frama-woocommerce"),
                "type"      => "number",
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_CARRIER_DEFAULT_EXPORT_INSURED_AMOUNT,
                "condition" => WCFRAMA_Settings::SETTING_CARRIER_DEFAULT_EXPORT_INSURED,
                "label"     => __("shipment_options_insured_amount", "frama-woocommerce"),
                "help_text" => __("shipment_options_insured_amount_help_text", "frama-woocommerce"),
                "type"      => "select",
                "options"   => WCFR_Data::getInsuranceAmounts(),
            ],
        ];
    }

    /**
     * @return array
     */
    private function get_section_export_defaults_main(): array
    {
        return [
            [
                "name"      => WCFRAMA_Settings::SETTING_SELECTED_CARRIER,
                "label"     => __("Carrier", "frama-woocommerce"),
                "type"      => "select",
                "options"   => WCFR_Data::CARRIERS_HUMAN,
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_SHIPPING_METHODS_PACKAGE_TYPES,
                "label"     => __("Package types", "frama-woocommerce"),
                "callback"  => [WCFR_Settings_Callbacks::class, "enhanced_select"],
                "loop"      => WCFR_Data::getPackageTypesHuman(),
                "options"   => (new WCFR_Shipping_Methods())->getShippingMethods(),
                "default"   => [],
                "help_text" => __(
                    "Select one or more shipping methods for each Frama package type",
                    "frama-woocommerce"
                ),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_LABEL_DESCRIPTION,
                "label"     => __("Reference field default value", "frama-woocommerce"),
                "help_text" => __(
                    "With this option you can add a description to the shipment. This will be printed on the top left of the label, and you can use this to search or sort shipments in your backoffice. Because of limited space on the label which varies per package type, we recommend that you keep the label description as short as possible.",
                    "frama-woocommerce"
                ),
                "append"  => $this->getLabelDescriptionAddition(),
            ],
        ];
    }

    /**
     * @return array
     */
    private function get_section_checkout_main(): array
    {
        return [
            [
                "name"      => WCFRAMA_Settings::SETTING_USE_SPLIT_ADDRESS_FIELDS,
                "label"     => __("Frama address fields", "frama-woocommerce"),
                "type"      => "toggle",
                "help_text" => __(
                    "When enabled the checkout will use the Frama address fields. This means there will be three separate fields for street name, number and suffix. Want to use the WooCommerce default fields? Leave this option unchecked.",
                    "frama-woocommerce"
                ),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_ENABLED,
                "label"     => __("Enable pickup point selection", "frama-woocommerce"),
                "type"      => "toggle",
                "help_text" => __(
                    "This will allow your customers to select whether they want their parcel delivered at home or to a pickup point.",
                    "frama-woocommerce"
                ),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_DISPLAY,
                "condition" => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_ENABLED,
                "label"     => __("settings_checkout_display_for", "frama-woocommerce"),
                "type"      => "select",
                "help_text" => __("settings_checkout_display_for_help_text", "frama-woocommerce"),
                "options"   => [
                    self::DISPLAY_FOR_SELECTED_METHODS => __("settings_checkout_display_for_selected_methods", "frama-woocommerce"),
                    self::DISPLAY_FOR_ALL_METHODS      => __("settings_checkout_display_for_all_methods", "frama-woocommerce"),
                ],
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_POSITION,
                "condition" => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_ENABLED,
                "label"     => __("Checkout position", "frama-woocommerce"),
                "type"      => "select",
                "default"   => "woocommerce_after_checkout_billing_form",
                "options"   => [
                    "woocommerce_after_checkout_billing_form"     => __(
                        "Show after billing details",
                        "frama-woocommerce"
                    ),
                    "woocommerce_after_checkout_shipping_form"    => __(
                        "Show after shipping details",
                        "frama-woocommerce"
                    ),
                    "woocommerce_checkout_after_customer_details" => __(
                        "Show after customer details",
                        "frama-woocommerce"
                    ),
                    "woocommerce_after_order_notes"               => __(
                        "Show after notes",
                        "frama-woocommerce"
                    ),
                    "woocommerce_review_order_before_payment"     => __(
                        "Show after subtotal",
                        "frama-woocommerce"
                    ),
                ],
                "help_text" => __(
                    "You can change the place of the delivery options on the checkout page. By default it will be placed after shipping details.",
                    "frama-woocommerce"
                ),
            ],
            [
                "name"              => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_CUSTOM_CSS,
                "condition"         => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_ENABLED,
                "label"             => __("Custom styles", "frama-woocommerce"),
                "type"              => "textarea",
                "append"            => $this->getCustomCssAddition(),
                "custom_attributes" => [
                    "style" => "font-family: monospace;",
                    "rows"  => "8",
                    "cols"  => "12",
                ],
            ],
        ];
    }

    /**
     * Get the weekdays from WP_Locale and remove any entries. Sunday is removed by default unless `null` is passed.
     *
     * @param int|null ...$remove
     *
     * @return array
     */
    private function getWeekdays(...$remove): array
    {
        $weekdays = (new WP_Locale())->weekday;

        if ($remove !== null) {
            $remove = count($remove) ? $remove : [0];
            foreach ($remove as $index) {
                unset($weekdays[$index]);
            }
        }

        return $weekdays;
    }

    private function get_section_checkout_strings(): array
    {
        return [
            [
                "name"      => WCFRAMA_Settings::SETTING_HEADER_DELIVERY_OPTIONS_TITLE,
                "condition" => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_ENABLED,
                "label"     => __("Delivery options title", "frama-woocommerce"),
                "title"     => "Delivery options title",
                "help_text" => __(
                    "You can place a delivery title above the Frama options. When there is no title, it will not be visible.",
                    "frama-woocommerce"
                ),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_DELIVERY_TITLE,
                "condition" => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_ENABLED,
                "label"     => __("Delivery title", "frama-woocommerce"),
                "default"   => __("Delivered at home or at work", "frama-woocommerce"),
            ],
            [
                "name"      => WCFRAMA_Settings::SETTING_PICKUP_TITLE,
                "condition" => WCFRAMA_Settings::SETTING_DELIVERY_OPTIONS_ENABLED,
                "label"     => __("Pickup title", "frama-woocommerce"),
                "default"   => __("Pickup", "frama-woocommerce"),
            ],
        ];
    }

    /**
     * Get the html string to render after the custom css select.
     *
     * @return string
     */
    private function getCustomCssAddition(): string
    {
        $currentTheme = wp_get_theme();

        $preset  = sanitize_title($currentTheme);
        $cssPath = WCFRAMA()->plugin_path() . "/assets/css/delivery-options/delivery-options-preset-$preset.css";

        if (! file_exists($cssPath)) {
            return "";
        }

        return sprintf(
            '<p>%s <a class="" href="#" onclick="document.querySelector(`#delivery_options_custom_css`).value = `%s`">%s</a></p>',
            sprintf(__('Theme "%s" detected.', "frama-woocommerce"), $currentTheme),
            file_get_contents($cssPath),
            __("Apply preset.", "frama-woocommerce")
        );
    }

    /**
     * Created html for clickable hints for the variables that can be used in the label description.
     *
     * @return string
     */
    private function getLabelDescriptionAddition(): string
    {
        $output = '';
        $variables = [
            '[ORDER_NR]'         => __('Order number', 'frama-woocommerce'),
            '[ORDER_ITEM_COUNT]' => __('Number of different products in order', 'frama-woocommerce'),
            '[ORDER_QTY_SUM]'    => __('Total quantity of all items in order', 'frama-woocommerce'),
            '[PRODUCT_IDS]'      => __('Product id\'s, comma-separated', 'frama-woocommerce'),
            '[PRODUCT_SKUS]'     => __('Product SKU\'s, comma-separated', 'frama-woocommerce'),
            '[PRODUCT_NAMES]'    => __('Product names, comma-separated', 'frama-woocommerce'),
            '[PRODUCT_QTYS]'     => __('Product quantities, comma-separated', 'frama-woocommerce'),
            '[PRODUCT_SKUSxQTY]' => __('Product SKU\'s x quantity, separated by spaces', 'frama-woocommerce'),
            '[PRODUCT_IDSxQTY]'  => __('Product id\'s x quantity, separated by spaces', 'frama-woocommerce'),
            '[PRODUCT_NAMESxQTY]'=> __('Product names x quantity, separated by spaces', 'frama-woocommerce'),
            '[CUSTOMER_NOTE]'    => __('Customer note', 'frama-woocommerce'),
        ];

        foreach ($variables as $variable => $description) {
            $output .= "<br><a onclick=\"var el = document.querySelector('#label_description_field input');el.value += '$variable';el.focus();\">$variable</a>: $description";
        }

        return sprintf("<div class=\"label-description-variables\"><p>%s: %s</p>", __('Available variables', 'frama-woocommerce'), $output);
    }

    /**
     * @param string $name
     * @param string|array  $conditions
     *
     * @return array
     */
    private static function getFeeField(string $name, array $conditions): array
    {
        return [
            "name"       => $name,
            "condition" => $conditions,
            "class"      => ["wcframa__child"],
            "label"      => __("Fee (optional)", "frama-woocommerce"),
            "type"       => "currency",
            "help_text"  => __(
                "Enter an amount that is either positive or negative. For example, do you want to give a discount for using this function or do you want to charge extra for this delivery option.",
                "frama-woocommerce"
            ),
        ];
    }
}

new WCFR_Settings_Data();
