<?php

use FramaNL\Support\Arr;

if (! defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

if (class_exists('WCFR_Rest')) {
    return;
}

/**
 * A simple JSON REST request abstraction layer
 */
class WCFR_Rest
{

    /**
     * Handle for the current cURL session
     *
     * @var
     */
    private $curl = null;

    /**
     * Default cURL settings
     *
     * @var
     */
    protected $curlDefaults = [
        // BOOLEANS
        CURLOPT_AUTOREFERER    => true,     // Update referer on redirects
        CURLOPT_FAILONERROR    => false,    // Return false on HTTP code > 400
        CURLOPT_FOLLOWLOCATION => false,    // DON'T Follow redirects
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FRESH_CONNECT  => true,     // Don't use cached connection
        CURLOPT_FORBID_REUSE   => true,     // Close connection

        // INTEGERS
        CURLOPT_TIMEOUT        => 10,       // cURL timeout
        CURLOPT_CONNECTTIMEOUT => 10,       // Connection timeout

        // STRINGS
        CURLOPT_ENCODING       => "",       // "identity", "deflate", and "gzip"
        CURLOPT_SSL_VERIFYPEER => false,    // if all else fails :)
    ];

    /**
     * Basic constructor
     * Checks for cURL and initialize options
     *
     * @return void
     * @throws Exception
     */
    public function __construct()
    {
        if (! function_exists("curl_init")) {
            throw new Exception("cURL is not installed on this system");
        }

        $this->curl = curl_init();
        if (! is_resource($this->curl) || ! isset($this->curl)) {
            throw new Exception("Unable to create cURL session");
        }

        $options                 = $this->curlDefaults;

        if ((ini_get('open_basedir') == '') AND (! ini_get('safe_mode'))) {
            $options[CURLOPT_FOLLOWLOCATION] = true;
        }

        $success = curl_setopt_array($this->curl, $options);
        if ($success !== true) {
            throw new Exception("cURL Error: " . curl_error($this->curl));
        }
    }

    /**
     * Closes the current cURL connection
     */
    public function close()
    {
        @curl_close($this->curl);
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * Returns last error message
     *
     * @return string  Error message
     */
    public function error()
    {
        return curl_error($this->curl);
    }

    /**
     * Returns last error code
     *
     * @return int
     */
    public function errno()
    {
        return curl_errno($this->curl);
    } // end function

    /**
     * @param       $url
     * @param array $headers
     * @param bool  $raw
     *
     * @return array
     * @throws Exception
     */
    public function get($url, $headers = [], $raw = false)
    {
        return $this->request($url, "GET", $headers, null, $raw);
    }

    /**
     * @param       $url
     * @param       $post
     * @param array $headers
     * @param bool  $raw
     *
     * @return array
     * @throws Exception
     */
    public function post($url, $post, $headers = [], $raw = false)
    {
        return $this->request($url, "POST", $headers, $post, $raw);
    }

    /**
     * @param       $url
     * @param       $body
     * @param array $headers
     * @param bool  $raw
     *
     * @return array
     * @throws Exception
     */
    public function put($url, $body, $headers = [], $raw = false)
    {
        return $this->request($url, "PUT", $headers, null, $raw);
    }

    /**
     * @param        $url
     * @param string $method
     * @param array  $headers
     * @param        $post
     * @param bool   $raw
     *
     * @return array
     * @throws Exception
     */
    public function request($url, $method = "GET", $headers = [], $post = null, $raw = false)
    {
        // Set the method and related options
        switch ($method) {
            case "PUT":
                throw new Exception('Can not put Frama shipment', 500);

            case "POST":
                $response = wp_remote_post($url, ['body' => $post, 'headers' => $headers]);
                break;

            case "DELETE":
                throw new Exception('Can not delete Frama shipment', 500);

            case "GET":
            default:
                if (isset($post)) {
                    $response = wp_remote_get($url, ['body' => $post, 'headers' => $headers]);
                }
                else {
                    $response = wp_remote_get($url, ['headers' => $headers]);
                }
                break;
        }

        // Close any open resource handle
        if (isset($f) && is_resource($f)) {
            @fclose($f);
        }

        $status = Arr::get($response, "response.code");
        $body   = Arr::get($response, "body");

        if ($raw !== true) {
            $body = json_decode($body, true); // The second parameter set to true returns objects as associative arrays
        }

        if ($status >= 400) {
            if ($raw === true) {
                $body = json_decode($body, true);
            }

            if (!empty($body["Errors"]) || !empty($body["errors"])) {
                $error = $this->parse_errors($body);
            } elseif (! empty($body["Error"])) {
                $error = $body["Error"];
            } elseif (! empty($body["error"])) {
                $error = $body["error"];
            } else {
                $error = "Unknown error: ".$status;
                if ($status==404) {
                    $error = 'Error 404 ' . $method. ' to ' . $url;
                }
            }
            throw new Exception($error, $status);
        }

        return ["code" => $status, "body" => $body, "headers" => Arr::get($response, "headers")
            //, "requestBody" => $post, "requestHeaders" => $headers, 'response' => $response,
        ];
    }

    /**
     * @param       $url
     * @param array $headers
     * @param bool  $raw
     *
     * @return array
     * @throws Exception
     */
    public function delete($url, $headers = [], $raw = false)
    {
        return $this->request($url, "GET", $headers, null, $raw);
    }

    /**
     * @param $body
     *
     * @return mixed|string
     */
    public function parse_errors($body)
    {
        $errors  = $body['Errors'] ?? $body['errors'];
        $message = $body['Error'] ?? $body['error'] ?? '';

        $parsed_errors = [];
        foreach ($errors as $error) {
            $code = $error['Code'] ?? $error['code'] ?? '';

            if (isset($error['human']) && is_array($error['human'])) {
                foreach ($error['human'] as $key => $human_error) {
                    $parsed_errors[$code] = "{$human_error} (<strong>Code {$code}</strong>)";
                }
            } elseif (isset($error['Description'])) {
                $parsed_errors[$code] = "{$error['Description']} (<strong>Code {$code}</strong>)";
            } elseif (isset($error['description'])) {
                $parsed_errors[$code] = "{$error['description']} (<strong>Code {$code}</strong>)";
            }   else {
                $parsed_errors[$code] = "{$message} (<strong>Code {$code}</strong>)";
            }
        }

        if (count($parsed_errors) == 1) {
            $html = array_shift($parsed_errors);
        } else {
            foreach ($parsed_errors as &$parsed_error) {
                $parsed_error = "<li>{$parsed_error}</li>";
            }
            $html = sprintf("<ul>%s</ul>", implode("\n", $parsed_errors));
        }

        return $html;
    }
}
