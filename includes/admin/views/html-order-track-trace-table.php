<?php

/**
 * This template is for the Track & Trace information in the Frama meta box in a single order/
 */

/**
 * @var array $consignments
 * @var int   $order_id
 * @var bool  $downloadDisplay
 */

$shipments = [];

try {
    $shipments = WCFRAMA()->export->getShipmentData($order_id);
} catch (Exception $e) {
    $message = $e->getMessage();
}

if (isset($message)) {
    echo "<p>$message</p>";
}

/**
 * Don't render the table if no shipments have been exported.
 */
if (! count($shipments)) {
    return;
}

?>

<table class="wcframa__table--track-trace">
  <thead>
  <tr>
    <th><?php _e("Track & Trace", "frama-woocommerce"); ?></th>
    <th><?php _e("Status", "frama-woocommerce"); ?></th>
    <th>&nbsp;</th>
  </tr>
  </thead>
  <tbody>
  <?php

  foreach ($shipments as $shipment_id => $shipment):
      ?>
    <tr>
      <td class="wcframa__order__track-trace">
          <?php WCFRAMA_Admin::renderTrackTraceLink($shipment); ?>
      </td>
      <td class="wcframa__order__status">
          <?php WCFRAMA_Admin::renderStatus($shipment) ?>
      </td>
      <td class="wcframa__td--create-label">
          <?php
          $action    = WCFR_Export::EXPORT;
          $getLabels = WCFR_Export::GET_LABELS;

          $order            = wc_get_order($order_id);

          WCFRAMA_Admin::renderAction(
              admin_url("admin-ajax.php?action=$action&request=$getLabels&shipment_ids=$shipment_id&order_ids=$order_id"),
              __("Print Frama label", "frama-woocommerce"),
              WCFRAMA()->plugin_url() . "/assets/img/print.svg"
          );
          ?>
      </td>
    </tr>
  <?php endforeach ?>
  </tbody>
</table>
