const babelify = require('babelify');
const browserify = require('browserify');
const buffer = require('vinyl-buffer');
const clean = require('gulp-clean');
const gulp = require('gulp');
const gulpPoSync = require('gulp-po-sync');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const tap = require('gulp-tap');
const uglify = require('gulp-uglify');
const wpPot = require('gulp-wp-pot');
const zip = require('gulp-zip');
const {exec} = require('child_process');

const PHP_FILES = ['*.php', 'migration/**/*.php', 'templates/**/*.php', 'includes/**/*.php'];

/**
 * Callback for use with tasks using child_process.exec().
 *
 * @param {Function} callback
 * @param {ExecException} err
 * @param {String} stdout
 * @param {String} stderr
 */
function execCallback(callback, err, stdout, stderr) {
  /* eslint-disable no-console */
  if (stdout) {
    console.log(stdout);
  }

  if (stderr) {
    console.warn(stderr);
  }
  /* eslint-enable no-console */
  if (typeof callback === 'function') {
    callback(err);
  }
}

/**
 * Empty the dist folder.
 */
gulp.task('clean', () => gulp.src('dist/**/*.*', {read: false})
  .pipe(clean({force: true})));

/**
 * Run babel on the javascript files.
 */
gulp.task('build:js', () => gulp.src('src/js/**/*.js', {read: false})
  .pipe(tap((file) => {
    file.contents = browserify(file.path)
      .transform(babelify)
      .bundle();
  }))
  .pipe(buffer())
  .pipe(sourcemaps.init())
  .pipe(uglify())
  .pipe(sourcemaps.write('./source-maps'))
  .pipe(gulp.dest('assets/js')));

/**
 * Compile and run postcss.
 */
gulp.task('build:scss', () => gulp.src('src/scss/**/*.scss')
  .pipe(sass())
  .pipe(postcss())
  .pipe(gulp.dest('assets/css')));

/**
 * Copy the static css files and images.
 */
gulp.task('copy', () => gulp.src([
  'src/css/**/*.*',
  'src/img/**/*.*',
], {
  base: 'src',
})
  .pipe(gulp.dest('assets')));

/**
 * Collect all files and put it in a zip file.
 */
gulp.task('zip', () => gulp.src([
  '*.png',
  'LICENSE',
  'assets/**/*',
  'includes/**/*',
  'languages/**/*',
  'readme.txt',
  'templates/**/*',
  'shipping/**/*',
  'frama-woocommerce.php',
  'wpm-config.json',
], {base: '.'})
  .pipe(zip('frama-woocommerce.zip'))
  .pipe(gulp.dest('./')));

/**
 * The default task.
 */
const build = gulp.series(
  'clean',
  gulp.parallel(
    'build:js',
    'build:scss',
    'copy',
  ),
);

gulp.task('build', build);
gulp.task('build:zip', gulp.series('build', 'zip'));

const watch = () => {
  gulp.watch(['src/css/**/*', 'src/img/**/*'], null, gulp.series('copy'));
  // Don't use babel in watch mode
  gulp.watch(['src/js/**/*'], null, () => gulp.src('src/js/**/*.js').pipe(gulp.dest('assets/js')));
  //gulp.watch(['node_modules/@frama/delivery-options/**/*'], null, gulp.series('copy:delivery-options'));
  gulp.watch(['src/scss/**/*'], null, gulp.series('build:scss'));
  //gulp.watch(['composer.json'], null, gulp.series('update:composer'));
  gulp.watch(['package.json'], null, gulp.series('update:npm'));
};

gulp.task('watch', gulp.series(
  build,
  watch,
));

exports.default = build;
