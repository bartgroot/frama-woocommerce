=== Plugin Name ===
Contributors: bartgroot
Tags: woocommerce, export, delivery, packages, frama
Requires at least: 3.5.1
Tested up to: 5.8.1
Stable tag: trunk
Requires PHP: 7.1
License: GPLv3 or later
License URI: http://www.opensource.org/licenses/gpl-license.php

Export your WooCommerce orders to Frama (www.frama.nl) and print labels directly from the WooCommerce admin

== Description ==
This WooCommerce extension allows you to export your orders to the Frama service (www.frama.nl). The products are delivered by PostNL, UPS or BPost.

= Main features =
- Pickup locations integrated in your checkout
- Export your WooCommerce orders to Frama with a simple click, single orders or in batch
- Print shipping labels directly (PDF)
- Choose your package type (Parcel or mailbox package)
- Define preset Frama shipping options (signature required, extra insurance, etc.)
- Modify the Frama shipping options per order before exporting
- Extra checkout fields to separate street name, house number and house number suffix for more precise address data
- Add Track & Trace link to the order confirmation email

An API-key is required for this plugin! You can find this in your Frama account.

== Installation ==

= Automatic installation =
Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't even need to leave your web browser. To do an automatic install of Frama WooCommerce, log in to your WordPress admin panel, navigate to the Plugins menu and click Add New.

In the search field type "Frama WooCommerce" and click Search Plugins. You can install it by simply clicking Install Now. After clicking that link you will be asked if you're sure you want to install the plugin. Click yes and WordPress will automatically complete the installation.

= Manual installation via the WordPress interface =
1. Download the plugin zip file to your computer
2. Go to the WordPress admin panel menu Plugins > Add New
3. Choose upload
4. Upload the plugin zip file, the plugin will now be installed
5. After installation has finished, click the 'activate plugin' link

= Manual installation via FTP =
1. Download the plugin file to your computer and unzip it
2. Using an FTP program, or your hosting control panel, upload the unzipped plugin folder to your WordPress installation's wp-content/plugins/ directory.
3. Activate the plugin from the Plugins menu within the WordPress admin.

= Setting up the plugin =
1. Go to the menu `WooCommerce > Frama`.
2. Fill in your API Details. If you don't have API details, log into your Frama account, you can find your API key under Instellingen → Algemeen.
3. Under 'Default export settings' you can set options that should be set by default for the export. You can change these settings per order at the time of export.
4. The plugin is ready to be used!

= Testing =
We advise you to test the whole checkout procedure once to see if everything works as it should. Pay special attention to the following:

The Frama plugin adds extra fields to the checkout of your webshop, to make it possible for the client to add street name, number and optional additions separately. This way you can be sure that everything is entered correctly. Because not all checkouts are configured alike, it's possible that the positioning/alignment of these extra fields have to be adjusted.

Moreover, after a label is created, a Track & Trace code is added to the order. When the order is completed from WooCommerce, this Track & Trace code is added to the email (when this is enabled in the settings). Check that the code is correctly displayed in your template. You can read how to change the text in the FAQ section.

== Frequently Asked Questions ==

= How do I get an API key? =
Please contact Frama.

= How do I change the Track & Trace email text? =
You can change the text (which is placed above the order details table by default) by applying the following filter:
`
add_filter( 'wcframa_email_text', 'wcframa_new_email_text' );
function wcframa_new_email_text($track_trace_tekst) {
	$nieuwe_tekst = 'Je kunt je bestelling volgen met het volgende PostNL Track & Trace nummer:';
	return $nieuwe_tekst;
}
`

== Screenshots ==

== Changelog ==

= 1.0.0 =
* First release.

= 1.0.1 =
* Small changes and bug fixes.

= 1.2.0 =
* Endpoint change to sandbox.
* 0 value for insurance removed
* Bug fix when frama address field house number is not empty
* Buf fix js script for opening pdf label file

= 1.2.1 =
* convert exported weight to grams according to unit setting in WooCommerce
* default weight for package is 10kg (for mailbox package remains 2kg)
* pickup-locations only available in Netherlands and Belgium
* settings for sending Track&Trace and Delivery email via Frama API

= 1.2.2 =
* weight will not be rounded down to whole kilograms
* default weight for shipments outside the Netherlands is 2kg
* pickup-location selector is now showing correctly after changing countries
* status automation after bulk export now works for all selected orders (not only the last)

= 1.2.3 =
* when country is changed and pickup-points selector is already selected and remains visible, deselect it so user has to select the radiobutton again in order to get pickup locations for the changed country

= 1.2.4 =
* don't save pickup location when selecting home delivery after first selecting pickup point

= 1.3.0 =
* Production release

= 1.3.1 =
* prevent fatal error with plugin Woocommerce Table Rate Shipping

== Upgrade Notice ==
