<?php
declare(strict_types=1);

namespace FramaNL\Factory;

use FramaNL\Model\Consignment\AbstractConsignment;
use FramaNL\Model\Consignment\BPostConsignment;
use FramaNL\Model\Consignment\PostNLConsignment;
use FramaNL\Model\Consignment\UPSConsignment;

class ConsignmentFactory
{
    public static function createByCarrierName(string $carrierName): AbstractConsignment
    {
        switch ($carrierName) {
            case UPSConsignment::CARRIER_NAME:
                return new UPSConsignment();
            case BPostConsignment::CARRIER_NAME:
                return new BPostConsignment();
            case PostNLConsignment::CARRIER_NAME:
                return new PostNLConsignment();
        }

        throw new \BadMethodCallException("Carrier name $carrierName not found");
    }
}
