<?php declare(strict_types=1);

namespace FramaNL\Factory;

use BadMethodCallException;
use FramaNL\Adapter\DeliveryOptions\AbstractDeliveryOptionsAdapter;
use FramaNL\Adapter\DeliveryOptions\DeliveryOptionsAdapter;
use FramaNL\Support\Arr;

class DeliveryOptionsAdapterFactory
{
    /**
     * @param array $deliveryOptionsData
     *
     * @return AbstractDeliveryOptionsAdapter
     * @throws Exception
     */
    public static function create(array $deliveryOptionsData): AbstractDeliveryOptionsAdapter
    {
        $deliveryOptionsData = Arr::fromObject($deliveryOptionsData);

        if (key_exists('deliveryType', $deliveryOptionsData)) {
            return new DeliveryOptionsAdapter($deliveryOptionsData);
        }

        throw new BadMethodCallException("Can't create DeliveryOptions. No suitable adapter found");
    }
}
