<?php

declare(strict_types=1);

namespace FramaNL\Model\Consignment;

use FramaNL\Exception\MissingFieldException;
use FramaNL\Model\FramaCustomsItem;
use FramaNL\Support\Helpers;

/**
 * A model of a consignment
 * Class Consignment
 */
class AbstractConsignment
{
    /**
     * Consignment types
     */
    public const DELIVERY_TYPE_MORNING  = 1;
    public const DELIVERY_TYPE_STANDARD = 2;
    public const DELIVERY_TYPE_EVENING  = 3;
    public const DELIVERY_TYPE_PICKUP   = 4;

    public const DELIVERY_TYPE_MORNING_NAME  = "morning";
    public const DELIVERY_TYPE_STANDARD_NAME = "standard";
    public const DELIVERY_TYPE_EVENING_NAME  = "evening";
    public const DELIVERY_TYPE_PICKUP_NAME   = "pickup";

    public const DELIVERY_TYPES_NAMES_IDS_MAP = [
        self::DELIVERY_TYPE_MORNING_NAME        => self::DELIVERY_TYPE_MORNING,
        self::DELIVERY_TYPE_STANDARD_NAME       => self::DELIVERY_TYPE_STANDARD,
        self::DELIVERY_TYPE_EVENING_NAME        => self::DELIVERY_TYPE_EVENING,
        self::DELIVERY_TYPE_PICKUP_NAME         => self::DELIVERY_TYPE_PICKUP,
    ];

    public const DEFAULT_DELIVERY_TYPE      = self::DELIVERY_TYPE_STANDARD;
    public const DEFAULT_DELIVERY_TYPE_NAME = self::DELIVERY_TYPE_STANDARD_NAME;

    /**
     * Customs declaration types
     */
    public const PACKAGE_CONTENTS_COMMERCIAL_GOODS   = 1;

    /**
     * Package types
     */
    public const PACKAGE_TYPE_PACKAGE       = 1;
    public const PACKAGE_TYPE_MAILBOX       = 2;
//    public const PACKAGE_TYPE_LETTER        = 3;
//    public const PACKAGE_TYPE_DIGITAL_STAMP = 4;

    public const PACKAGE_TYPE_PACKAGE_NAME       = "package";
    public const PACKAGE_TYPE_MAILBOX_NAME       = "mailbox";

    public const PACKAGE_TYPES_IDS = [
        self::PACKAGE_TYPE_PACKAGE,
        self::PACKAGE_TYPE_MAILBOX,
    ];

    public const PACKAGE_TYPES_NAMES = [
        self::PACKAGE_TYPE_PACKAGE_NAME,
        self::PACKAGE_TYPE_MAILBOX_NAME,
    ];

    public const PACKAGE_TYPES_NAMES_IDS_MAP = [
        self::PACKAGE_TYPE_PACKAGE_NAME       => self::PACKAGE_TYPE_PACKAGE,
        self::PACKAGE_TYPE_MAILBOX_NAME       => self::PACKAGE_TYPE_MAILBOX,
    ];

    public const DEFAULT_PACKAGE_TYPE_NAME = self::PACKAGE_TYPE_PACKAGE_NAME;

    /**
     * Regular expression used to make sure the date is correct.
     */
    public const DATE_REGEX        = '~(\d{4}-\d{2}-\d{2})$~';
    public const DATE_TIME_REGEX   = '~(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})$~';
    public const MAX_STREET_LENGTH = 40;

    public const CC_NL = 'NL';
    public const CC_BE = 'BE';

    /**
     * @var array
     */
    public const INSURANCE_POSSIBILITIES_LOCAL = [];

    /**
     * @var string
     */
    protected $local_cc = '';

    /**
     * @internal
     * @var string
     */
    public $reference_identifier;

    /**
     * @internal
     * @var int
     */
    public $consignment_id;

    /**
     * @internal
     * @var string|null
     */
    public $api_key;

    /**
     * @internal
     * @var string|null
     */
    public $barcode;

    /**
     * @internal
     * @var int
     */
    public $status = null;

    /**
     * @internal
     * @var integer
     */
    public $shop_id;

    /**
     * @internal
     * @var string
     */
    public $cc;

    /**
     * @internal
     * @var string
     */
    public $city;

    /**
     * @internal
     * @var string
     */
    public $street;

    /**
     * @internal
     * @var string|null
     */
    public $number;

    /**
     * @internal
     * @var string
     */
    public $number_suffix = '';


    /**
     * @internal
     * @var string
     */
    public $postal_code;

    /**
     * @internal
     * @var string
     */
    public $person;

    /**
     * @internal
     * @var string
     */
    public $company = '';

    /**
     * @internal
     * @var string
     */
    public $email = '';

    /**
     * @internal
     * @var string
     */
    public $phone = '';

    /**
     * @internal
     * @var integer
     */
    public $package_type;

    /**
     * @internal
     * @var integer
     */
    public $delivery_type = self::DEFAULT_DELIVERY_TYPE;

    /**
     * @internal
     * @var boolean
     */
    public $only_recipient;

    /**
     * @internal
     * @var boolean
     */
    public $signature;

    /**
     * @internal
     * @var boolean
     */
    public $return;

    /**
     * @internal
     * @var boolean
     */
    public $large_format;

    /**
     * @internal
     * @var boolean
     */
    public $age_check;


    /**
     * @internal
     * @var int
     */
    public $insurance = 0;

    /**
     * @internal
     * @var int
     */
    public $contents = self::PACKAGE_CONTENTS_COMMERCIAL_GOODS;

    /**
     * @internal
     * @var string
     */
    public $invoice;

    /**
     * @internal
     * @var array
     */
    public $items = [];

    /**
     * @var bool
     */
    private $save_recipient_address = true;

    /**
     * @var Helpers
     */
    private $helper;

    public function __construct()
    {
        $this->helper = new Helpers();
    }

    /**
     * Get the status of the consignment
     * Pattern: [1 – 99]<br>
     * Example:
     *          1 pending - concept
     *          2 pending - registered
     *          3 enroute - handed to carrier
     *          4 enroute - sorting
     *          5 enroute - distribution
     *          6 enroute - customs
     *          7 delivered - at recipient
     *          8 delivered - ready for pickup
     *          9 delivered - package picked up
     *          10 delivered - return shipment ready for pickup
     *          11 delivered - return shipment package picked up
     *          12 printed - letter
     *          13 credit
     *          30 inactive - concept
     *          31 inactive - registered
     *          32 inactive - enroute - handed to carrier
     *          33 inactive - enroute - sorting
     *          34 inactive - enroute - distribution
     *          35 inactive - enroute - customs
     *          36 inactive - delivered - at recipient
     *          37 inactive - delivered - ready for pickup
     *          38 inactive - delivered - package picked up
     *          99 inactive - unknown
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * Status of the consignment
     *
     * @param int $status
     *
     * @return AbstractConsignment
     * @internal
     */
    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry()
    {
        return $this->cc;
    }

    /**
     * The address country code
     * ISO3166-1 alpha2 country code<br>
     * <br>
     * Pattern: [A-Z]{2}<br>
     * Example: NL, BE, CW<br>
     * Required: Yes
     *
     * @param string $cc
     *
     * @return $this
     */
    public function setCountry($cc)
    {
        $this->cc = $cc;

        return $this;
    }



    /**
     * @return bool
     */
    public function isSaveRecipientAddress(): bool
    {
        return $this->save_recipient_address;
    }


    /**
     * Street number suffix.
     * Required: no
     *
     * @param string|null $numberSuffix
     *
     * @return $this
     */
    public function setNumberSuffix(?string $numberSuffix): self
    {
        $this->number_suffix = $numberSuffix;

        return $this;
    }

    /**
     * Company name
     * Required: no
     *
     * @param string|null $company
     *
     * @return $this
     */
    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * The address email
     * Required: no
     *
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param int|null $default
     *
     * @return int|null
     */
    public function getPackageType($default = null): ?int
    {
        return $this->package_type ?? $default;
    }

    /**
     * The package type
     * For international shipment only package type 1 is allowed
     * Pattern: [1 – 3]<br>
     * Example:
     *          1. package
     *          2. mailbox package
     *          3. letter
     * Required: Yes
     *
     * @param int $packageType
     *
     * @return $this
     */
    public function setPackageType(int $packageType): self
    {
        $this->package_type = $packageType;

        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryType(): int
    {
        return $this->delivery_type;
    }

    /**
     * The delivery type for the package
     *
     * @param int  $deliveryType
     *
     * @return AbstractConsignment
     */
    public function setDeliveryType(int $deliveryType): self
    {
        $this->delivery_type = $deliveryType;

        return $this;
    }


    /**
     * @return bool
     */
    public function isOnlyRecipient(): bool
    {
        return false;
    }

    /**
     * Deliver the package to the recipient only
     * Required: No
     *
     * @param bool $only_recipient
     *
     * @return $this
     */
    public function setOnlyRecipient(bool $only_recipient): self
    {
        if ($only_recipient) {
            throw new BadMethodCallException('Only recipient has to be false in ' . static::class);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isSignature(): bool
    {
        return false;
    }

    /**
     * * Package must be signed for
     * Required: No
     *
     * @param bool $signature
     *
     * @return AbstractConsignment
     */
    public function setSignature(bool $signature): self
    {
        if ($signature) {
            throw new BadMethodCallException('Signature has to be false in ' . static::class);
        }

        return $this;
    }

    /**
     * Return the package if the recipient is not home
     * Required: No
     *
     * @param bool $return
     *
     * @return $this
     * @throws Exception
     */
    public function setReturn(bool $return): self
    {
        $this->return = $this->canHaveOption($return);

        return $this;
    }

    /**
     * @return bool
     */
    public function isLargeFormat(): bool
    {
        return false;
    }

    /**
     * Large format package
     * Required: No
     *
     * @param bool $largeFormat
     *
     * @return $this
     */
    public function setLargeFormat(bool $largeFormat): self
    {
        if ($largeFormat) {
            throw new BadMethodCallException('Large format has to be false in ' . static::class);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function hasAgeCheck(): bool
    {
        return false;
    }

    /**
     * Age check
     * Required: No
     *
     * @param bool $ageCheck
     *
     * @return AbstractConsignment
     */
    public function setAgeCheck(bool $ageCheck): self
    {
        if ($ageCheck) {
            throw new BadMethodCallException('Age check has to be false in ' . static::class);
        }

        return $this;
    }


    /**
     * @return int
     */
    public function getInsurance(): int
    {
        return $this->insurance;
    }

    /**
     * Insurance price for the package.
     * Composite type containing integer and currency. The amount is without decimal separators.
     * Required: No
     *
     * @param int|null $insurance
     *
     * @return AbstractConsignment
     * @throws Exception
     */
    public function setInsurance(?int $insurance): self
    {
        if (null === $insurance) {
            $this->insurance = null;

            return $this;
        }

        if (empty(static::INSURANCE_POSSIBILITIES_LOCAL)) {
            throw new \BadMethodCallException('Property insurance_possibilities_local not found in ' . static::class);
        }

        if (empty($this->local_cc)) {
            throw new \BadMethodCallException('Property local_cc not found in ' . static::class);
        }

        if (! in_array($insurance, static::INSURANCE_POSSIBILITIES_LOCAL) && $this->getCountry() == $this->local_cc) {
            throw new \BadMethodCallException(
                'Insurance must be one of ' . implode(', ', static::INSURANCE_POSSIBILITIES_LOCAL)
            );
        }

        if (! $this->canHaveOption()) {
            $insurance = 0;
        }

        $this->insurance = $insurance;

        return $this;
    }



    /**
     * @return integer
     */
    public function getContents(): int
    {
        return $this->contents;
    }

    /**
     * The type of contents in the package.
     * The package contents are only needed in case of shipping outside EU,
     * this is mandatory info for customs form.
     * Pattern: [1 - 5]
     * Example: 1. commercial goods
     *          2. commercial samples
     *          3. documents
     *          4. gifts
     *          5. return shipment
     * Required: Yes for shipping outside EU
     *
     * @param int $contents
     *
     * @return $this
     */
    public function setContents(int $contents): self
    {
        $this->contents = $contents;

        return $this;
    }


    /**
     * @return FramaCustomsItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * A CustomsItem objects with description in the package.
     * Required: Yes for international shipments
     *
     * @param FramaCustomsItem $item
     *
     * @return $this
     * @throws MissingFieldException
     */
    public function addItem(FramaCustomsItem $item): self
    {
        $item->ensureFilled();

        $this->items[] = $item;

        return $this;
    }

    /**
     * Only package type 1 can have extra options
     *
     * @param $option
     *
     * @return bool
     * @throws MissingFieldException
     */
    protected function canHaveOption(bool $option = true): bool
    {
        if ($this->getPackageType() === null) {
            throw new MissingFieldException('Set package type before ' . $option);
        }

        return $this->getPackageType() == self::PACKAGE_TYPE_PACKAGE ? $option : false;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        return true;
    }
}
