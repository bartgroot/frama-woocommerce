<?php declare(strict_types=1);

namespace FramaNL\Model\Consignment;

use Exception;
use FramaNL\Exception\MissingFieldException;

class UPSConsignment extends AbstractConsignment
{
    /**
     * @var int
     */
    public const CARRIER_ID = 40;

    /**
     * @var string
     */
    public const CARRIER_NAME = 'ups';

    /**
     * @var array
     */
    public const INSURANCE_POSSIBILITIES_LOCAL = [        
        100,
        250,
        500,
        1000,
        1500,
        2000,
        2500,
        3000,
        3500,
        4000,
        4500,
        5000
    ];

    /**
     * @var array
     */
    private const VALID_PACKAGE_TYPES = [
        self::PACKAGE_TYPE_PACKAGE,
        self::PACKAGE_TYPE_MAILBOX,
    ];

    /**
     * @var string
     */
    protected $local_cc = self::CC_NL;

    /**
     * Street number suffix.
     *
     * Required: no
     *
     * @param string|null $numberSuffix
     *
     * @return $this
     */
    public function setNumberSuffix(?string $numberSuffix): AbstractConsignment
    {
        $this->number_suffix = $numberSuffix;

        return $this;
    }

    /**
     * The package type
     *
     * For international shipment only package type 1 is allowed
     * Pattern: [1 – 3]<br>
     * Example:
     *          1. package
     *          2. mailbox package
     *          3. letter
     * Required: Yes
     *
     * @param int $packageType
     *
     * @return $this
     * @throws Exception
     */
    public function setPackageType(int $packageType): AbstractConsignment
    {
        if (! in_array($packageType, self::VALID_PACKAGE_TYPES)) {
            throw new Exception('Use the correct package type for shipment:' . $this->consignment_id);
        }

        return parent::setPackageType($packageType);
    }


    /**
     * Insurance price for the package.
     *
     * Composite type containing integer and currency. The amount is without decimal separators.
     * Required: No
     *
     * @param int|null $insurance
     *
     * @return AbstractConsignment
     * @throws Exception
     */
    public function setInsurance(?int $insurance): AbstractConsignment
    {
        if (null === $insurance) {
            throw new \BadMethodCallException('Insurance must be one of ' . implode(', ', self::INSURANCE_POSSIBILITIES_LOCAL));
        }

        return parent::setInsurance($insurance);
    }

    /**
     * @return bool
     */
    public function isOnlyRecipient(): bool
    {
        return $this->only_recipient;
    }

    /**
     * Deliver the package to the recipient only
     *
     * Required: No
     *
     * @param bool $only_recipient
     *
     * @return $this
     * @throws Exception
     */
    public function setOnlyRecipient(bool $only_recipient): AbstractConsignment
    {
        $this->only_recipient = $this->canHaveOption($only_recipient);

        return $this;
    }

    /**
     * @return bool
     */
    public function isSignature(): bool
    {
        return $this->signature;
    }

    /**
     * Package must be signed for
     *
     * Required: No
     *
     * @param bool $signature
     *
     * @return $this
     * @throws Exception
     */
    public function setSignature(bool $signature): AbstractConsignment
    {
        $this->signature = $this->canHaveOption($signature);

        return $this;
    }

    /**
     * @return boolean
     */
    public function isLargeFormat(): bool
    {
        return $this->large_format;
    }

    /**
     *  * Large format package
     *
     * Required: No
     *
     * @param bool $largeFormat
     *
     * @return AbstractConsignment
     * @throws MissingFieldException
     */
    public function setLargeFormat(bool $largeFormat): AbstractConsignment
    {
        $this->large_format = $this->canHaveOption($largeFormat);

        return $this;
    }

    /**
     * @return bool
     */
    public function hasAgeCheck(): bool
    {
        return $this->age_check;
    }

    /**
     * Age check
     *
     * Required: No
     *
     * @param bool $ageCheck
     *
     * @return $this
     * @throws Exception
     */
    public function setAgeCheck(bool $ageCheck): AbstractConsignment
    {
        $this->age_check = $this->canHaveOption($ageCheck);

        return $this;
    }

}
