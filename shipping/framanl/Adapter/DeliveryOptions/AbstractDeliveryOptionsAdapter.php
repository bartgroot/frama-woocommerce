<?php declare(strict_types=1);

namespace FramaNL\Adapter\DeliveryOptions;

use FramaNL\Model\Consignment\AbstractConsignment;

abstract class AbstractDeliveryOptionsAdapter
{
    /**
     * @var string
     */
    protected $date;

    /**
     * @var string
     */
    protected $deliveryType;

    /**
     * @var string
     */
    protected $packageType;

    /**
     * @var AbstractShipmentOptionsAdapter|null
     */
    protected $shipmentOptions;

    /**
     * @var string|null
     */
    protected $carrier;

    /**
     * @return string
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getDeliveryType(): ?string
    {
        return $this->deliveryType;
    }

    /**
     * @return string
     */
    public function getPackageType(): ?string
    {
        return $this->packageType;
    }

    /**
     * @return AbstractShipmentOptionsAdapter|null
     */
    public function getShipmentOptions(): ?AbstractShipmentOptionsAdapter
    {
        return $this->shipmentOptions;
    }

    /**
     * @return string
     */
    public function getCarrier(): ?string
    {
        return $this->carrier;
    }

    /**
     * @return bool
     */
    public function isPickup(): bool
    {
        if ($this->deliveryType === null) {
            return false;
        }

        return $this->deliveryType == AbstractConsignment::DELIVERY_TYPE_PICKUP_NAME;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            "carrier"         => $this->getCarrier(),
            "date"            => $this->getDate(),
            "deliveryType"    => $this->getDeliveryType(),
            "packageType"     => $this->getPackageType(),
            "isPickup"        => $this->isPickup(),
            "shipmentOptions" => $this->getShipmentOptions() ? $this->getShipmentOptions()->toArray() : null,
        ];
    }
}
