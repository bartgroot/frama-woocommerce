<?php declare(strict_types=1);

namespace FramaNL\Exception;

class InvalidConsignmentException extends \Exception
{
}
