<?php declare(strict_types=1);

namespace FramaNL\Exception;

class ApiException extends \Exception
{
}
