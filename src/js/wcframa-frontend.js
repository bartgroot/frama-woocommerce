/**
 * The following jsdoc blocks are for declaring the types of the injected variables from php.
 */

/**
 * @property {Object} FramaDisplaySettings
 * @property {String} FramaDisplaySettings.isUsingSplitAddressFields
 * @property {String[]} FramaDisplaySettings.splitAddressFieldsCountries
 *
 * @see \wcfr_checkout::inject_delivery_options_variables
 */

/**
 * @property {Object} wcfr
 * @property {String} wcfr.ajax_url
 *
 * @see \wcfr_checkout::inject_delivery_options_variables
 */

/**
 * @property {Object} FramaDeliveryOptions
 * @property {String} FramaDeliveryOptions.allowedShippingMethods
 * @property {String} FramaDeliveryOptions.disallowedShippingMethods
 * @property {String} FramaDeliveryOptions.hiddenInputName
 * @see \wcfr_checkout::inject_delivery_options_variables
 */
/* eslint-disable-next-line max-lines-per-function */
jQuery(($) => {
  // eslint-disable-next-line no-var
  var FramaFrontend = {
    /**
     * Whether the delivery options are currently shown or not. Defaults to true and can be set to false depending on
     *  shipping methods.
     *
     * @type {Boolean}
     */
    hasDeliveryOptions: true,

    /**
     * @type {RegExp}
     */
    splitStreetRegex: /(.*?)\s?(\d{1,4})[/\s-]{0,2}([A-z]\d{1,3}|-\d{1,4}|\d{2}\w{1,2}|[A-z][A-z\s]{0,3})?$/,

    /**
     * @type {Boolean}
     */
    isUsingSplitAddressFields: Boolean(Number(FramaDisplaySettings.isUsingSplitAddressFields)),

    /**
     * @type {String[]}
     */
    splitAddressFieldsCountries: FramaDisplaySettings.splitAddressFieldsCountries,

    /**
     * @type {Array}
     */
    allowedShippingMethods: JSON.parse(FramaDeliveryOptions.allowedShippingMethods),

    /**
     * @type {Array}
     */
    disallowedShippingMethods: JSON.parse(FramaDeliveryOptions.disallowedShippingMethods),

    /**
     * @type {Boolean}
     */
    alwaysShow: Boolean(parseInt(FramaDeliveryOptions.alwaysShow)),

    /**
     * @type {Object<String, String>}
     */
    previousCountry: {},

    /**
     * @type {?String}
     */
    selectedShippingMethod: null,

    /**
     * @type {Element}
     */
    hiddenDataInput: null,

    /**
     * @type {String}
     */
    addressType: null,

    /**
     * Ship to different address checkbox.
     *
     * @type {String}
     */
    shipToDifferentAddressField: '#ship-to-different-address-checkbox',

    /**
     * Shipping method radio buttons.
     *
     * @type {String}
     */
    shippingMethodField: '[name="shipping_method[0]"]',

    /**
     * Highest shipping class field.
     *
     * @type {String}
     */
    highestShippingClassField: '[name="frama_highest_shipping_class"]',

    addressField: 'address_1',
    cityField: 'city',
    countryField: 'country',
    countryRow: 'country_field',
    houseNumberField: 'house_number',
    houseNumberSuffixField: 'house_number_suffix',
    postcodeField: 'postcode',
    streetNameField: 'street_name',

    /**
     * Delivery options events.
     */
    updateDeliveryOptionsEvent: 'frama_update_delivery_options',
    updatedDeliveryOptionsEvent: 'frama_updated_delivery_options',
    updatedAddressEvent: 'frama_updated_address',

    showDeliveryOptionsEvent: 'frama_show_delivery_options',
    hideDeliveryOptionsEvent: 'frama_hide_delivery_options',
    updateConfigEvent: 'frama_update_config',

    /**
     * WooCommerce checkout events.
     */
    countryToStateChangedEvent: 'country_to_state_changed',
    updateWooCommerceCheckoutEvent: 'update_checkout',
    updatedWooCommerceCheckoutEvent: 'updated_checkout',

    /**
     * Initialize the script.
     */
    init() {
      FramaFrontend.addListeners();
      FramaFrontend.injectHiddenInput();
    },

    /**
     * When the delivery options are updated, fill the hidden input with the new data and trigger the WooCommerce
     *  update_checkout event.
     *
     * @param {CustomEvent} event - The update event.
     */
    onDeliveryOptionsUpdate(event) {
      let value = '';

      if (event.detail !== null) {
        value = JSON.stringify(event.detail);
      }

      FramaFrontend.hiddenDataInput.value = value;

      /**
       * Remove this event before triggering and re-add it after because it will cause an infinite loop otherwise.
       */
      $(document.body).off(FramaFrontend.updatedWooCommerceCheckoutEvent, FramaFrontend.updateShippingMethod);
      FramaFrontend.triggerEvent(FramaFrontend.updateWooCommerceCheckoutEvent);

      const restoreEventListener = () => {
        $(document.body).on(FramaFrontend.updatedWooCommerceCheckoutEvent, FramaFrontend.updateShippingMethod);
        $(document.body).off(FramaFrontend.updatedWooCommerceCheckoutEvent, restoreEventListener);
      };

      $(document.body).on(FramaFrontend.updatedWooCommerceCheckoutEvent, restoreEventListener);

      /**
       * After the "updated_checkout" event the shipping methods will be rendered, restore the event listener and delete
       *  this one in the process.
       */
      $(document.body).on(FramaFrontend.updatedWooCommerceCheckoutEvent, restoreEventListener);
    },

    /**
     * If split fields are used add house number to the fields. Otherwise use address line 1.
     *
     * @returns {String}
     */
    getSplitField() {
      return FramaFrontend.hasSplitAddressFields()
        ? FramaFrontend.houseNumberField
        : FramaFrontend.addressField;
    },

    /**
     * Add all event listeners.
     */
    addListeners() {
      FramaFrontend.addAddressListeners();
      FramaFrontend.updateShippingMethod();

      const addressCheckbox = $(FramaFrontend.shipToDifferentAddressField).val();

      if (addressCheckbox) {
        document
          .querySelector(FramaFrontend.shipToDifferentAddressField)
          .addEventListener('change', FramaFrontend.addAddressListeners);
      }

      document.addEventListener(FramaFrontend.updatedAddressEvent, FramaFrontend.onDeliveryOptionsAddressUpdate);
      document.addEventListener(FramaFrontend.updatedDeliveryOptionsEvent, FramaFrontend.onDeliveryOptionsUpdate);

      /*
       * jQuery events.
       */
      $(document.body).on(FramaFrontend.countryToStateChangedEvent, FramaFrontend.synchronizeAddress);
      $(document.body).on(FramaFrontend.countryToStateChangedEvent, FramaFrontend.updateAddress);
      $(document.body).on(FramaFrontend.updatedWooCommerceCheckoutEvent, FramaFrontend.updateShippingMethod);
    },

    /**
     * Get field by name. Will return element with FramaFrontend selector: "#<billing|shipping>_<name>".
     *
     * @param {String} name - The part after `shipping/billing` in the id of an element in WooCommerce.
     * @param {?String} addressType - "shipping" or "billing".
     *
     * @returns {Element}
     */
    getField(name, addressType = FramaFrontend.addressType) {
      if (!addressType) {
        addressType = FramaFrontend.getAddressType();
      }

      const selector = `#${addressType}_${name}`;
      const field = document.querySelector(selector);

      if (!field) {
        // eslint-disable-next-line no-console
        console.warn(`Field ${selector} not found.`);
      }

      return field;
    },

    /**
     * Update address type.
     *
     * @returns {String}
     */
    getAddressType: function() {
      let useShipping = false;
      const addressCheckbox = document.querySelector(FramaFrontend.shipToDifferentAddressField);

      if (addressCheckbox) {
        useShipping = document.querySelector(FramaFrontend.shipToDifferentAddressField).checked;
      }

      FramaFrontend.addressType = useShipping ? 'shipping' : 'billing';

      return FramaFrontend.addressType;
    },

    /**
     * Get the house number from either the house_number field or the address_1 field. If it's the address field use
     * the split street regex to extract the house number.
     *
     * @returns {String}
     */
    getHouseNumber() {
      const hasBillingNumber = $(`#billing_${FramaFrontend.houseNumberField}`).val() !== '';
      const hasShippingNumber = $(`#shipping_${FramaFrontend.houseNumberField}`).val() !== '';
      const hasNumber = hasBillingNumber || hasShippingNumber;

      if (FramaFrontend.hasSplitAddressFields() && hasNumber) {
        return FramaFrontend.getField(FramaFrontend.houseNumberField).value;
      }

      return FramaFrontend.getAddressParts().house_number;
    },

    /**
     * @returns {{house_number_suffix: (String | null), house_number: (String | null), street_name: (String | null)}}
     */
    getAddressParts: function() {
      const address = FramaFrontend.getField(FramaFrontend.addressField).value;
      const result = FramaFrontend.splitStreetRegex.exec(address);

      const parts = {};

      parts[FramaFrontend.streetNameField] = result ? result[1] : null;
      parts[FramaFrontend.houseNumberField] = result ? result[2] : null;
      parts[FramaFrontend.houseNumberSuffixField] = result ? result[3] : null;

      return parts;
    },

    /**
     * Trigger an event on a given element. Defaults to body.
     *
     * @param {String} identifier - Name of the event.
     * @param {String|HTMLElement|Document} [element] - Element to trigger from. Defaults to 'body'.
     */
    triggerEvent(identifier, element) {
      const event = document.createEvent('HTMLEvents');
      event.initEvent(identifier, true, false);
      element = !element || typeof element === 'string' ? document.querySelector(element || 'body') : element;
      element.dispatchEvent(event);
    },

    /**
     * Check if the country changed by comparing the old value with the new value before overwriting the FramaConfig
     *  with the new value. Returns true if none was set yet.
     *
     * @returns {Boolean}
     */
    countryHasChanged() {
      if (window.FramaConfig.address && window.FramaConfig.address.hasOwnProperty('cc')) {
        return window.FramaConfig.address.cc !== FramaFrontend.getField(FramaFrontend.countryField).value;
      }

      return true;
    },

    /**
     * Get data from form fields, put it in the global FramaConfig, then trigger updating the delivery options.
     */
    updateAddress() {
      FramaFrontend.validateFramaConfig();

      window.FramaConfig.address = {
        cc: FramaFrontend.getField(FramaFrontend.countryField).value,
        postalCode: FramaFrontend.getField(FramaFrontend.postcodeField).value,
        number: FramaFrontend.getHouseNumber(),
        city: FramaFrontend.getField(FramaFrontend.cityField).value,
      };

      if (FramaFrontend.hasDeliveryOptions) {
        FramaFrontend.triggerEvent(FramaFrontend.updateDeliveryOptionsEvent);
      }
    },

    /**
     * Set the values of the WooCommerce fields from delivery options data.
     *
     * @param {?Object} address - The new address.
     * @param {String} address.postalCode
     * @param {String} address.city
     * @param {String} address.number
     */
    setAddressFromDeliveryOptions: function(address = null) {
      address = address || {};

      if (address.postalCode) {
        FramaFrontend.getField(FramaFrontend.postcodeField).value = address.postalCode;
      }

      if (address.city) {
        FramaFrontend.getField(FramaFrontend.cityField).value = address.city;
      }

      if (address.number) {
        FramaFrontend.setHouseNumber(address.number);
      }
    },

    /**
     * Set the values of the WooCommerce fields. Ignores empty values.
     *
     * @param {Object|null} address - The new address.
     */
    fillCheckoutFields: function(address) {
      if (!address) {
        return;
      }

      Object
        .keys(address)
        .forEach((fieldName) => {
          const field = FramaFrontend.getField(fieldName);
          const value = address[fieldName];

          if (!field || !value) {
            return;
          }

          field.value = value;
        });
    },

    /**
     * Set the house number.
     *
     * @param {String|Number} number - New house number to set.
     */
    setHouseNumber(number) {
      const address = FramaFrontend.getField(FramaFrontend.addressField).value;
      const oldHouseNumber = FramaFrontend.getHouseNumber();

      if (FramaFrontend.hasSplitAddressFields()) {
        if (oldHouseNumber) {
          FramaFrontend.getField(FramaFrontend.addressField).value = address.replace(oldHouseNumber, number);
        } else {
          FramaFrontend.getField(FramaFrontend.addressField).value = address + number;
        }
      } else {
        FramaFrontend.getField(FramaFrontend.houseNumberField).value = number;
      }
    },

    /**
     * Create an input field in the checkout form to be able to pass the checkout data to the $_POST variable when
     * placing the order.
     *
     * @see includes/class-wcfr-checkout.php::save_delivery_options();
     */
    injectHiddenInput() {
      FramaFrontend.hiddenDataInput = document.createElement('input');
      FramaFrontend.hiddenDataInput.setAttribute('hidden', 'hidden');
      FramaFrontend.hiddenDataInput.setAttribute('name', FramaDeliveryOptions.hiddenInputName);

      document.querySelector('form[name="checkout"]').appendChild(FramaFrontend.hiddenDataInput);
    },

    /**
     * When the delivery options module has updated the address, using the "retry" option.
     *
     * @param {CustomEvent} event - The event containing the new address.
     */
    onDeliveryOptionsAddressUpdate: function(event) {
      FramaFrontend.setAddressFromDeliveryOptions(event.detail);
    },

    /**
     * Update the shipping method to the new selections. Triggers hiding/showing of the delivery options.
     */
    updateShippingMethod() {
      let shippingMethod;
      const shippingMethodField = document.querySelectorAll(FramaFrontend.shippingMethodField);
      const selectedShippingMethodField = document.querySelector(`${FramaFrontend.shippingMethodField}:checked`);

      /**
       * Check if shipping method field exists. It doesn't exist if there are no shipping methods available for the
       *  current address/product combination or in general.
       *
       * If there is no shipping method the delivery options will always be hidden.
       */
      if (shippingMethodField.length) {
        shippingMethod = selectedShippingMethodField ? selectedShippingMethodField.value : shippingMethodField[0].value;

        /**
         * This shipping method will have a suffix in the checkout, but this is not present in the array of
         *  selected shipping methods from the SETTING_DELIVERY_OPTIONS_DISPLAY setting.
         *
         * All variants of flat_rate (including shipping classes) do already have their suffix set properly.
         */
        if (shippingMethod.indexOf('flat_rate') === 0) {
          const shippingClass = FramaFrontend.getHighestShippingClass();

          if (shippingClass) {
            shippingMethod = `flat_rate:${shippingClass}`;
          }
        }
      } else {
        shippingMethod = null;
      }

      if (shippingMethod !== FramaFrontend.selectedShippingMethod) {
        FramaFrontend.onChangeShippingMethod(FramaFrontend.selectedShippingMethod, shippingMethod);
        FramaFrontend.selectedShippingMethod = shippingMethod;
      }
    },

    /**
     * Hides/shows the delivery options based on the current shipping method. Makes sure to not update the checkout
     *  unless necessary by checking if hasDeliveryOptions is true or false.
     */
    toggleDeliveryOptions(shippingMethod) {
      if (FramaFrontend.shippingMethodHasDeliveryOptions(shippingMethod)) {
        FramaFrontend.hasDeliveryOptions = true;
        FramaFrontend.triggerEvent(FramaFrontend.showDeliveryOptionsEvent, document);
        FramaFrontend.updateDeliveryOptionsConfig();
      } else {
        FramaFrontend.hasDeliveryOptions = false;
        FramaFrontend.triggerEvent(FramaFrontend.hideDeliveryOptionsEvent, document);
      }
    },

    sendUpdateConfigEvent() {
      FramaFrontend.triggerEvent(FramaFrontend.updateConfigEvent);
    },

    /**
     * Check if the given shipping method is allowed to have delivery options by checking if the name starts with any
     * value in a list of shipping methods.
     *
     * Most of the values in this list will be full shipping method names, with an instance id, but some can't have one.
     * That's the reason we're checking if it starts with this value instead of whether it's equal.
     *
     * @param {?String} shippingMethod
     * @returns {Boolean}
     */
    shippingMethodHasDeliveryOptions(shippingMethod = FramaFrontend.getSelectedShippingMethod()) {
      let display = false;
      let invert = false;
      let list = FramaFrontend.allowedShippingMethods;

      if (!shippingMethod) {
        return false;
      }

      if (shippingMethod.indexOf('free_shipping') === 0) {
        shippingMethod = 'free_shipping';
      }

      /**
       * If "all" is selected for allowed shipping methods check if the current method is NOT in the
       *  disallowedShippingMethods array.
       */
      if (FramaFrontend.alwaysShow) {
        list = FramaFrontend.disallowedShippingMethods;
        invert = true;
      }

      list.forEach((method) => {
        const currentMethodIsAllowed = shippingMethod.indexOf(method) > -1;

        if (currentMethodIsAllowed) {
          display = true;
        }
      });

      if (invert) {
        display = !display;
      }

      return display;
    },

    /**
     * Add listeners to the address fields remove them before adding new ones if they already exist, then update
     *  shipping method and delivery options if needed.
     *
     * Uses the country field's parent row because there is no better way to catch the select2 (or selectWoo) events as
     *  we never know when the select is loaded and can't add a normal change event. The delivery options has a debounce
     *  function on the update event so it doesn't matter if we send 5 updates at once.
     */
    addAddressListeners() {
      const fields = [FramaFrontend.countryField, FramaFrontend.postcodeField, FramaFrontend.getSplitField()];

      /* If address type is already set, remove the existing listeners before adding new ones. */
      if (FramaFrontend.addressType) {
        fields.forEach((field) => {
          FramaFrontend.getField(field).removeEventListener('change', FramaFrontend.updateAddress);
        });
      }

      FramaFrontend.getAddressType();

      fields.forEach((field) => {
        FramaFrontend.getField(field).addEventListener('change', FramaFrontend.updateAddress);
      });

      FramaFrontend.updateAddress();
    },

    /**
     * Get the current shipping method without the shipping class.
     *
     * @returns {String}
     */
    getShippingMethodWithoutClass() {
      let shippingMethod = FramaFrontend.selectedShippingMethod;
      const indexOfSemicolon = shippingMethod.indexOf(':');

      shippingMethod = shippingMethod.substring(0, indexOfSemicolon === -1 ? shippingMethod.length : indexOfSemicolon);

      return shippingMethod;
    },

    /**
     * Get the highest shipping class by doing a call to WordPress. We're getting it this way and not from the
     *  highest_shipping_class input because that causes some kind of timing issue which makes the delivery options not
     *  show up.
     *
     * @returns {String|null}
     */
    getHighestShippingClass() {
      let shippingClass = null;

      $.ajax({
        type: 'POST',
        url: wcframa.ajax_url,
        async: false,
        data: {
          action: 'get_highest_shipping_class',
        },
        success(data) {
          shippingClass = data;
        },
      });

      return shippingClass;
    },

    /**
     * Fetch and update the delivery options config. For use with changing shipping methods, for example, as doing so
     *  changes the prices of delivery and any extra options.
     */
    updateDeliveryOptionsConfig() {
      FramaFrontend.validateFramaConfig();
      $.ajax({
        type: 'GET',
        url: wcframa.ajax_url,
        async: false,
        data: {
          action: 'wcframa_get_delivery_options_config',
        },
        success(data) {
          const {config} = JSON.parse(data);
          window.FramaConfig.config = config;
          FramaFrontend.sendUpdateConfigEvent();
        },
      });
    },

    /**
     * @returns {?String}
     */
    getSelectedShippingMethod() {
      let shippingMethod = FramaFrontend.selectedShippingMethod;

      if (shippingMethod === 'flat_rate') {
        shippingMethod += `:${document.querySelectorAll(FramaFrontend.highestShippingClassField).length}`;
      }

      return shippingMethod;
    },

    /**
     * Sync addresses between split and non-split address fields.
     *
     * @param {Event} event
     * @param {String} newCountry
     */
    synchronizeAddress(event, newCountry) {
      if (!FramaFrontend.isUsingSplitAddressFields) {
        return;
      }

      const data = $('form').serializeArray();

      ['shipping', 'billing'].forEach((addressType) => {
        if (!FramaFrontend.hasAddressType(addressType)) {
          return;
        }

        const typeCountry = data.find((item) => item.name === `${addressType}_country`);
        const hasAddressTypeCountry = FramaFrontend.previousCountry.hasOwnProperty(addressType);
        const countryChanged = FramaFrontend.previousCountry[addressType] !== newCountry;

        if (!hasAddressTypeCountry || countryChanged) {
          FramaFrontend.previousCountry[addressType] = typeCountry.value;
        }

        if (!countryChanged) {
          return;
        }
        jQuery('#frama_deliver_to_customer').click();

        if (FramaFrontend.hasSplitAddressFields(newCountry)) {
          const parts = FramaFrontend.getAddressParts();

          FramaFrontend.fillCheckoutFields(parts);
        } else {
          const [
            houseNumberField,
            houseNumberSuffixField,
            streetNameField,
          ] = [
            FramaFrontend.houseNumberField,
            FramaFrontend.houseNumberSuffixField,
            FramaFrontend.streetNameField,
          ].map((fieldName) => FramaFrontend.getField(fieldName));

          const number = houseNumberField.value || '';
          const street = streetNameField.value || '';
          const suffix = houseNumberSuffixField.value || '';

          FramaFrontend.fillCheckoutFields({
            address_1: `${street} ${number}${suffix}`.trim(),
          });
        }

        FramaFrontend.updateAddress();
      });
    },

    /**
     * @param {?String} country
     *
     * @returns {Boolean}
     */
    hasSplitAddressFields: function(country = null) {
      if (!country) {
        country = FramaFrontend.getField(FramaFrontend.countryField).value;
      }

      if (!FramaFrontend.isUsingSplitAddressFields) {
        return false;
      }

      return FramaFrontend.splitAddressFieldsCountries.includes(country.toUpperCase());
    },

    /**
     * Checks if the inner wrapper of an address type form exists to determine if the address type is available.
     *
     * Does not check the outer div (.woocommerce-shipping-fields) because when the shipping form does not exist, it's
     *  still rendered on the page.
     *
     * @param {String} addressType
     * @returns {Boolean}
     */
    hasAddressType(addressType) {
      const formWrapper = document.querySelector(`.woocommerce-${addressType}-fields__field-wrapper`);

      return Boolean(formWrapper);
    },

    /**
     * @param {?String} oldShippingMethod
     * @param {?String} newShippingMethod
     */
    onChangeShippingMethod(oldShippingMethod, newShippingMethod) {
      FramaFrontend.toggleDeliveryOptions(newShippingMethod);
      // if (FramaFrontend.shippingMethodHasDeliveryOptions(newShippingMethod)) {
      //   FramaFrontend.updateDeliveryOptionsConfig();
      // }
    },

    validateFramaConfig() {
      if (!window.hasOwnProperty('FramaConfig')) {
        throw 'window.FramaConfig not found!';
      }

      if (typeof window.FramaConfig === 'string') {
        window.FramaConfig = JSON.parse(window.FramaConfig);
      }
    },
  };

  window.FramaFrontend = FramaFrontend;
  FramaFrontend.init();
});
