function frama_bind_radio_buttons() {
    jQuery('.frama_pickup_location_radio').click(function (event) {
        if (localStorage) {
            localStorage.framaPickupLocation = event.target.id;
        }
    });
}
jQuery('body').on('updated_checkout', function(){
    let $ = jQuery;
    let frama_delivery_options = $('#frama_delivery_options');
    let pickupLocationAvailable = isFramaPickupLocationAvailable();
    if (!frama_delivery_options.is(":empty") && !pickupLocationAvailable) {
        frama_delivery_options.html('');
        return;
    }
    if (frama_delivery_options.is(":empty") && pickupLocationAvailable) {
        frama_delivery_options.html(
            '<h3>' + FramaConfig.strings.headerDeliveryOptions + '</h3>'+
            '<input type="radio" id="frama_deliver_to_customer" name="frama_delivery_destination" value="address"> '+
            '<label for="frama_deliver_to_customer">' + FramaConfig.strings.deliveryTitle + '</label><br>'+
            '<input type="radio" id="frama_deliver_to_pickup_point" name="frama_delivery_destination" value="pickup" disabled> '+
            '<label for="frama_deliver_to_pickup_point">' + FramaConfig.strings.pickupTitle + '</label><br>'+
            '<div><div id="frama_pickup_locations_placeholder" style="display:none"></div></div>');
    }
    if (!frama_delivery_options.is(":empty")) {
        let pickupPointRadio = $('#frama_deliver_to_pickup_point');
        let deliverToCustomerRadio = $('#frama_deliver_to_customer');
        if (pickupLocationAvailable) {
            pickupPointRadio.removeAttr('disabled');
        } else {
            pickupPointRadio.attr('disabled', 'disabled');
        }
        deliverToCustomerRadio.click(function () {
            $('#frama_pickup_locations_placeholder').fadeOut();
            $('[name="_frama_pickup_location"]:checked').prop('checked', false);
            if (localStorage && 'framaPickupLocation' in localStorage) {
                localStorage.removeItem('framaPickupLocation');
            }
        });

        pickupPointRadio.click(function () {
            $('#frama_pickup_locations_placeholder').fadeIn();
            let postcode = null;
            let cc = null;
            //postcode = $('#'+FramaFrontend.addressType+'_'+FramaFrontend.postcodeField).val();
            if (FramaConfig && FramaConfig.address) {
                postcode = FramaConfig.address.postalCode;
                cc = FramaConfig.address.cc;
            }
            if (postcode) {
                $("#frama_pickup_locations_placeholder").html('<span id="orderstatus"><img src="/wp-admin/images/loading.gif" class="spinner loader-image"/>&nbsp;</span>');
                $.ajax({
                    type: "post",
                    url: wcframa.ajax_url,
                    data: "action=frama_get_pickup_locations&cc=" + cc + "&postcode=" + postcode,
                    success: function (data) {
                        $("#frama_pickup_locations_placeholder").html(data+" <br/><button type='button' id='frama_retry_button'>"+FramaConfig.strings.retry+"</button>");
                        $("#frama_retry_button").click(function() { pickupPointRadio.click(); });
                        frama_bind_radio_buttons();
                        if (localStorage && 'framaPickupLocation' in localStorage) {
                            $('#'+localStorage.framaPickupLocation).attr('checked', true);
                        }
                    },
                    error: function (xhr, status) {
                        $("#frama_pickup_locations_placeholder").html(status+" <button type='button' id='frama_retry_button'>"+FramaConfig.strings.retry+"</button>");
                        $("#frama_retry_button").click(function() { pickupPointRadio.click(); });
                    }
                });
            } else {
                $("#frama_pickup_locations_placeholder").html(FramaConfig.strings.addressNotFound);
            }
        });
        if ($('#frama_pickup_locations_placeholder').is(":hidden")) { //delayed click to load pickup locations when the address is known
            if (!pickupPointRadio.attr('disabled') && localStorage && 'framaPickupLocation' in localStorage) {
                pickupPointRadio.attr('checked', true);
                pickupPointRadio.click();
            } else {
                deliverToCustomerRadio.attr('checked', true);
            }
        }
    }
});

function isFramaPickupLocationAvailable() {
    if (FramaDeliveryOptions.allowedShippingMethods && Array.isArray(FramaDisplaySettings.pickupLocationCountries)) {
        if (window.FramaConfig.address) {
            let country = window.FramaConfig.address.cc;
            if (country && FramaDisplaySettings.pickupLocationCountries.indexOf(country) < 0) {
                return false;
            }
        }
        let arrayOfAllowedShippingMethods = eval(FramaDeliveryOptions.allowedShippingMethods); //parse the string to an array
        if (Array.isArray(arrayOfAllowedShippingMethods)) {
            if (arrayOfAllowedShippingMethods.length == 0)
                return true;
            let highestShipping = jQuery('input[name="frama_highest_shipping_class"]').first().attr('value');
            if (highestShipping) {
                for (const allowedShippingMethod of arrayOfAllowedShippingMethods) {
                    if (highestShipping === allowedShippingMethod.split(':')[1])
                        return true;
                }
            }
        }
    }
    return false;
}
